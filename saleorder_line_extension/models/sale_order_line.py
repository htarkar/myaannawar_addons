from odoo import fields, models, api
from odoo.exceptions import UserError

class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'

	purchase_order = fields.Many2one('stock.picking', 'Goods Receive', required=True)
	number = fields.Char('Number', default = 0)

	def get_product_picking(self):
		return [i.id for i in self.env['stock.picking'].search(
			[('state', '=', 'done'), ('sale_id', '=', False)]).filtered(
			lambda s: self.product_id.id in [p.product_id.id for p in s.move_lines])]

	@api.onchange('purchase_order', 'product_id')
	def get_shipment_product(self):
		domain = [('id', 'in', [p.id for p in self.purchase_order.move_lines.mapped('product_id')])] if self.purchase_order else []
		if not self.product_id.id:
			# picking_domain = [('id', 'in', [ i.id for i in self.env['stock.picking'].search([]).filtered(lambda s: any([ i.qty_received > i.qty_invoiced for i in s.purchase_id.order_line ]))])]
			picking_domain = [('id', 'in', [ i.id for i in self.env['stock.picking'].search([('sale_id', '=', False)]).filtered(lambda s: any([ i.qty_received < 1 or i.qty_received != i.qty_invoiced for i in s.purchase_id.order_line ])) ])]

		else:
			picking_domain = [('id', 'in', self.get_product_picking() if len(self.get_product_picking()) > 0 else [])]
		# return {'domain': {'product_id': domain, 'purchase_order': picking_domain}}
		# print('picking_domain', picking_domain)
		return {'domain': {'product_id': domain, 'purchase_order': picking_domain }}

	@api.onchange('product_uom_qty', 'product_uom', 'route_id')
	def _onchange_product_id_check_availability(self):
		res = super(SaleOrderLine, self)._onchange_product_id_check_availability()
		res = {}
		return res

	# @api.onchange('product_uom_qty')
	# def check_shipment_qty(self):
	# 	if self.purchase_order.id and self.product_id.id and self.product_uom_qty > self.purchase_order.move_lines.filtered(lambda l: l.product_id.id == self.product_id.id).remaining_qty:
	# 		raise UserError('You plan to sell %s %s but you only have %s available in %s .' %(self.product_uom_qty, self.product_uom.name, self.purchase_order.move_lines.filtered(lambda l: l.product_id.id == self.product_id.id).remaining_qty, self.purchase_order.name))

class StockPicking(models.Model):
	_inherit = 'stock.picking'

	def auto_assign_value(self):
		for line in self.move_lines:
			if not line.quantity_done:
				line.quantity_done = line.product_uom_qty

	@api.model
	def name_search(self, name='', args=None, operator='ilike', limit=100):
		print('name_search', name, operator, self.purchase_id, self.purchase_id.partner_ref, self.sale_id)
		if args is None:
			args = []
		pickings = self.browse()
		results = False
		if name :
			results = self.search(['|', ('name', '=', name), ('partner_id.name', '=', name)] + args, limit=limit)
		if not results:
			results = self.search(['|', ('name', operator, name), ('partner_id.name', operator, name)] + args, limit=limit)
		if not results:
			results = self.search(['|', ('origin', operator, name), ('purchase_id.partner_ref', operator, name)] + args, limit=limit)
		if not results and name:
			results = self.search([('sale_id', '=', False)]).filtered(lambda s: name.lower() in ' '.join(
				[p.product_id.product_tmpl_id.display_name.lower() for p in s.move_lines]))
		return results.name_get()

	@api.multi
	def name_get(self):
		res = []
		for rec in self:
			# print('rec', rec, rec.purchase_id, rec.purchase_id.id, rec.purchase_id.partner_ref)
			res.append((rec.id, '%s - %s' %(rec.partner_id.name, rec.purchase_id.partner_ref if rec.purchase_id.partner_ref else rec.name)))
		# print('res', res)
		return res

class PurchaseOrder(models.Model):
	_inherit = 'purchase.order.line'

	number = fields.Char('Number', required=True)

class ResPartner(models.Model):
	_inherit = 'res.partner'

	@api.multi
	def name_get(self):
		res = []
		for rec in self:
			res.append((rec.id, '%s  %s' %(rec.name, "[ %s ]"%(rec.city) if rec.city else "")))
		return res
