{
    'name': "Sale Order line Extension(Myaannawar)",

    'description': """
        User Setting Confirmation. 
    """,

    'author': "Kanaung Myanmar",
    'website': "http://www.yourcompany.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','sale','purchase', 'account'],

    'data': [
        'views/sale_order_line_view.xml'
    ],
    'installable': True,
}