# -*- encoding: utf-8 -*-
{
    'name': 'Dynamic Master Import',
    'category': 'Master',
    'version': '0.1',
    'author': 'Kanaung Myanmar Software Solution',
    'summary': 'Master Data Import',
    'application': 'True',
    'depends': [
                    'base', 
                    'base_import',
                    'stock'
                ],
    'data': [
                'views/master_import.xml'
            ],
}
