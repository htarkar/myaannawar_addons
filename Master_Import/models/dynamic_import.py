from xlrd import open_workbook
from odoo.tools.translate import _
import base64
import logging
from datetime import datetime
from decimal import Decimal
from odoo import models,fields
_logger = logging.getLogger(__name__)

class DynamicImport(models.Model):
    _name = 'dynamic.import' 
    _description =  'Master Import'

    name = fields.Char('Description', required=True)
    import_date = fields.Date('Import Date', readonly=True, default = lambda s: fields.Date.context_today(s))
    import_fname = fields.Char('Filename', size=128, required=True)
    import_file = fields.Binary('File', required=True)
    note = fields.Text('Log')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('completed', 'Completed'),
        ('error', 'Error'),
    ], 'States', default = 'draft')
    target_object = fields.Many2one('ir.model', string = 'Object')

    err_log = ''
    total_record = 0

    def _check_file_extension(self):
        for import_file in self:
            return import_file.import_fname.lower().endswith('.xls')  or import_file.import_fname.lower().endswith('.xlsx')

    _constraints = [(_check_file_extension, "Please import microsoft excel (.xlsx or .xls) file!", ['import_fname'])]

    # ## Load excel data file
    def get_excel_datas(self, sheets):
        result = []
        for s in sheets:
            # # header row
            headers = []
            header_row = 0
            for hcol in range(0, s.ncols):
                headers.append(s.cell(header_row, hcol).value)
                            
            result.append(headers)
            
            # # value rows
            for row in range(header_row + 1, s.nrows):
                values = []
                for col in range(0, s.ncols):
                    #   *** Data Type ***
                    # XL_CELL_EMPTY   0   empty string ‘’
                    # XL_CELL_TEXT    1   a Unicode string
                    # XL_CELL_NUMBER  2   float
                    # XL_CELL_DATE    3   float
                    # XL_CELL_BOOLEAN 4   int; 1 means True, 0 means False
                    # XL_CELL_ERROR   5   int representing internal Excel codes; for a text representation, refer to the supplied dictionary error_text_from_code
                    # XL_CELL_BLANK   6   empty string ‘’. Note: this type will appear only when open_workbook(..., formatting_info= True) is used.
                    value = s.cell(row, col).value
                    
                    if s.cell(row, col).ctype in [1, 2]:
                        #
                        #   Excel cell data_type has no int and 
                        #   always get int data as float when read with xlrd module.
                        #   So, just rechange those data to int
                        #
                        try:
                            if int(float(value)) == float(value):
                                value = int(float(value))
                        except:
                            pass

                    values.append(value)
                result.append(values)
            self.total_record = len(result) -1

        return result
    
    def get_headers(self, header_row):
        #
        #   Get header row name and field types to comfortable with dynamic import
        #
        name_row = []
        field_types = {}
        for ind in range(len(header_row)):
            field = [ (i.name, [i.ttype, i.relation]) for i in self.target_object.field_id if i.field_description == header_row[ind]]
            # print('field', field)
            try:
                name_row.append(field[0][0])                # Add to header row
                field_types[field[0][0]] = field[0][1]      # Store field type
            except IndexError as e:
                self.err_log += _("Field '%s' does not exist !") % header_row[ind] + '\n'
        return [name_row, field_types]

    def set_state_to_error(self, err_log):
        self.write({'note': err_log, 'state': 'error'})

    def check_dupliate(self, model_name, import_vals):
        #
        #   Check duplicate record by using regular field, Mostly 'name' field
        #
        model = self.env[model_name]
        result = []

        if 'name' in import_vals and model_name != 'product.pricelist.item':
            #   For most models
            try:
                if model.search_count([('name', '=', import_vals['name'])]) > 0:
                    result.append(model.search([('name', '=', import_vals['name'])]))
            except:
                pass
        if model_name == 'product.pricelist.item':
            if 'product_id' in import_vals and import_vals['product_id']:
                #   For pricelistitem, name field does not store, so search using product_id or categ_id
                if model.search_count([('product_id', '=', import_vals['product_id'])]) > 0:
                    result.append(model.search([('product_id', '=', import_vals['product_id'])]))
            elif 'categ_id' in import_vals and import_vals['categ_id']:
                #   For pricelistitem, name field does not store, so search using product_id or categ_id
                if model.search_count([('categ_id', '=', import_vals['categ_id'])]) > 0:
                    result.append(model.search([('categ_id', '=', import_vals['categ_id'])]))
        return result

    def err_box(self, imported_record, value, name):
        self.err_log += _("Wrong value '%s' for '%s' field.!") % (value, name) + '\n'
        # self.err_log += _("Total Records           : %d") % self.total_record + '\n'
        # self.err_log += _("Imported Records    : %d") % imported_record + '\n'
        # self.err_log += _("Remaining Record    : %d") % (self.total_record - imported_record) + '\n'

    def import_data(self):  
        import_file = self.import_file
        lines = base64.decodestring(import_file)
        wb = open_workbook(file_contents=lines)
        excel_rows = self.get_excel_datas(wb.sheets())
        base_object = self.env[self.target_object.model]
        
        imported_record = 0
        updated_record = 0

        vals_dict = {
            True: ['True', '1', '1.0', 1.0, 1, True],
            False: ['False', '0', '0.0', 0.0, 0, False, '', ' ']
        }

        header_row = excel_rows[0]          # First row already considered as header row
        result = self.get_headers(header_row)
        header_row, field_types = result[0], result[1]
        #
        #   Check for error and return if exist
        #
        if self.err_log:
            self.set_state_to_error(self.err_log)
            return

        for line in self.web_progress_iter(excel_rows[1:], msg="Importing Records ..."):
            import_vals = {}
            for name, value in list(zip(header_row, line)):
                if field_types.get(name)[0] in ['many2one', 'one2many', 'many2many']:
                    #
                    #   For relation type, get ID 
                    #
                    try:
                        record = self.env[field_types.get(name)[1]].search([('name', '=', str(value).strip())])
                        if not record and field_types.get(name)[1] == 'product.category':
                            record = self.env[field_types.get(name)[1]].search([('complete_name', '=', str(value).strip())])
                        if not record:
                            #
                            #   For relation fields, something they can't be used 0, False to skip values
                            #
                            try:
                                value = [ key for key in vals_dict if value in vals_dict[key] ][0]
                            except:
                                self.err_box(imported_record, value, name)
                        else:
                            value = record.id
                    except ValueError as e:
                        self.err_box(imported_record, value, name)
                    
                elif field_types.get(name)[0] == 'selection':
                    type_dict = dict(base_object._fields[name].selection)
                    try:
                        value = [i for i in type_dict if type_dict[i] == value][0]
                    except IndexError as e:
                        self.err_box(imported_record, value, name)

                elif field_types.get(name)[0] == 'boolean':
                    try:
                        value = [ key for key in vals_dict if value in vals_dict[key] ][0]
                    except:
                        self.err_box(imported_record, value, name)
                import_vals[name] = value
            #
            #   Check again for error and return if exist or create row
            #
            if self.err_log:
                self.set_state_to_error(self.err_log)
                return
            else:
                duplicate_item = self.check_dupliate(self.target_object.model, import_vals)
                if duplicate_item:
                    duplicate_item[0].write(import_vals)
                    updated_record += 1
                    continue
                item = base_object.create(import_vals)
                imported_record += 1

        note = 'Import Success at ' + str(datetime.strptime(datetime.today().strftime('%Y-%m-%d %H:%M:%S'),
                  '%Y-%m-%d %H:%M:%S'))+ '\n' + \
                    'Total Records           : ' + str(self.total_record) + '\n' + \
                    'Imported Records    : ' + str(imported_record) + '\n' + \
                    'Updated Records     : ' + str(updated_record) + '\n' + \
                    'Remaining Records : ' + str((self.total_record - (imported_record + updated_record)))

        self.write({'state': 'completed','note': note})
            
