{
    'name': "State Extension(Myaannawar)",

    'description': """
        User Setting Confirmation. 
    """,

    'author': "Kanaung Myanmar",
    'website': "http://www.yourcompany.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base','base_setup', 'purchase'],

    'data': [
        'views/state_view.xml'
    ],
    'installable': True,
}