from odoo import models, fields, api
from ast import literal_eval
from odoo.exceptions import ValidationError

class StateCode(models.Model):
	_inherit = 'res.country.state'

	padding = fields.Integer("Length",default = 5, required = True)
	preview = fields.Char('Preview', readonly = True)

	@api.constrains('name')
	def checkStateNames(self):
		check = self.search([('name', '=ilike', self.name.strip()), ('id', "!=", self.id)])
		if(len(check) > 0) and \
			check.name.strip() == self.name.strip():
			raise ValidationError('The name of the state must be unique!')
		# if(self.name_search(self.name))


	@api.onchange('code', 'padding')
	def show_preview(self):
		self.preview = "{}{}".format(self.code or "", ("#" * self.padding) or "") if self.code else ""

class UserSetting(models.TransientModel):
	_inherit = "res.config.settings"

	auto_generade_user_ref = fields.Boolean(default = False, string = 'User Internal Reference')

	@api.model
	def get_values(self):
		#
		#	Get values of setting when refresh
		#
		res = super(UserSetting, self).get_values()
		params = self.env['ir.config_parameter'].sudo()

		res.update(
			auto_generade_user_ref=params.get_param('state_extension.auto_generade_user_ref', default=False),
		)
		return res

	def set_values(self):
		#
		#	Save setting when user make something changes
		#
		super(UserSetting, self).set_values()
		ref = self.auto_generade_user_ref

		self.env['ir.config_parameter'].sudo().set_param('state_extension.auto_generade_user_ref', ref)

class PurchaseOrder(models.Model):
	_inherit = 'purchase.order'

	@api.multi
	def auto_generadeRef(self, state):
		sh_code = state.code
		pad_leng = state.padding
		try :
			next_code = self.search_count([('partner_ref', '=like', "%s%s"%(sh_code, '%'))]) + 1
			print('next_code', next_code)

			next_code = ("0" * (pad_leng - len(str(next_code)))) + str(next_code)
		except:
			next_code = ("0" * (pad_leng-1)) + "1"
		if sh_code:
			next_str = "%s%s"%(sh_code, next_code)
			print('next_str', next_str)
			return next_str
		return False

	@api.onchange('partner_id')
	def autoFillRef(self):
		if self.partner_id:
			result = self.auto_generadeRef(self.partner_id.state_id)
			self.partner_ref = result if result else ''

	# @api.model
	# def create(self,vals):
	# 	res = super(ResPartner, self).create(vals)
	#
	# 	check = self.env['ir.config_parameter'].sudo().get_param('state_extension.auto_generade_user_ref', default='False')
	# 	print("Check", check, check is True)
	# 	# d
	# 	if check or check is True:
	# 		print('Check', check, res.state_id.id)
	# 		if not res.state_id.id:
	# 			raise UserError('"State" must not be blank when you had allowed to auto generade internal refernence in creating partner !')
	# 		elif not res.state_id.code or not res.state_id.padding:
	# 			raise UserError('Both short code and length must be exist in state !')
	# 		print('--> ', self.auto_generade(res.state_id))
	# 		res.write({'ref' : self.auto_generade(res.state_id)})
	# 	# d
	# 	return res

class ResCountry(models.Model):
	_inherit = 'res.country'

	active = fields.Boolean(default = True)

class ResPartner(models.Model):
	_inherit = 'res.partner'

	@api.model
	def defaultMyanmar(self):
		return self.env["res.country"].search([('name', '=', 'Myanmar')]).id or False

	country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', default = defaultMyanmar)

	@api.onchange('country_id')
	def hide_all(self):
		hide = self.env['res.country'].search([('name', '!=' , 'Myanmar')])
		hide.write({ 'active': False })
		# [i.write({ 'active': False}) for i in hide]
