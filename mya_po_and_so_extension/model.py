from odoo import models, fields

class Currency(models.Model):
	_inherit = 'purchase.order'

	READONLY_STATES = {
		'purchase': [('readonly', True)],
		'done': [('readonly', True)],
		'cancel': [('readonly', True)],}

	def _default_currency(self):
		print("Called")
		return self.env['res.currency'].search([('id', '=', 120)])

	currency_id = fields.Many2one('res.currency', 'Currency', required=True, states=READONLY_STATES,\
		default=_default_currency)