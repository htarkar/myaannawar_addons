from odoo import models, fields, api

class AccountInvoiceLine(models.Model):
	_inherit = 'account.invoice.line'

	number = fields.Char(related = 'purchase_line_id.number', string = 'Number')
	so_number = fields.Char(string = 'Number', default = 0)
	shipment_id = fields.Many2one('stock.picking', 'Good Receive', null = True)

	@api.model
	def create(self, values):
		print("Line values", values)
		res = super(AccountInvoiceLine, self).create(values)
		if 'so_number' in values and values.get('so_number'):
			#
			#	For so invoice
			#
			try:
				res.so_number = values.get("so_number")
			except:
				pass
		return res


