# -*- coding: utf-8 -*-

from odoo import models, fields, api

class StockMove(models.Model):
	_inherit = 'stock.move'

	number = fields.Char('Number', default = 0)

	@api.model
	def create(self, values):
		res = super(StockMove, self).create(values)
		#
		#	Add number field
		#
		res.write({'number': res.purchase_line_id.number if res.purchase_line_id.id else res.sale_line_id.number})
		return res
		