from odoo import models, fields

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    is_outstanding = fields.Boolean(default = False)