from odoo import models, fields, api

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.onchange('partner_id')
    def autoFillRef(self):
        if self.partner_id:
            self.partner_ref = self.partner_id.ref
