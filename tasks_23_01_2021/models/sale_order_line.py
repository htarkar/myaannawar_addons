from odoo import models, fields, api

class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'

	@api.multi
	def _prepare_invoice_line(self, qty):
		res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
		res['so_number'] = self.number
		res['shipment_id'] = self.purchase_order.id
		return res