# -*- coding: utf-8 -*-

from odoo import models, fields, api

class AccountInvoiceDiscount(models.Model):
	_name = "account.invoice.discount"
	_description = "Invoice Discount"

	invoice_id = fields.Many2one('account.invoice', string='Invoice', ondelete='cascade', index=True)
	name = fields.Char(string='Description', required=True)
	amount = fields.Monetary()
	currency_id = fields.Many2one('res.currency', related='invoice_id.currency_id', store=True, readonly=True)
