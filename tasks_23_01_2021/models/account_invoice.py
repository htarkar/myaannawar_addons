# -*- coding: utf-8 -*-

from odoo import models, fields, api
from myanmar import language, encodings
import platform
import logging
import json

_logger = logging.getLogger(__name__)

myan_code = [
'က',
'ခ',
'ဂ',
'ဃ',
'င',
'စ',
'ဆ',
'ဇ',
'ဈ',
'ဉ',
'ည',
'ဋ',
'ဌ',
'ဍ',
'ဎ',
'ဏ',
'တ',
'ထ',
'ဒ',
'ဓ',
'န',
'ပ',
'ဖ',
'ဗ',
'ဘ',
'မ',
'ယ',
'ရ',
'လ',
'ဝ',
'သ',
'ဟ',
'ဠ',
'အ',
]

myan_code = [
'က',
'ခ',
'ဂ',
'ဃ',
'င',
'စ',
'ဆ',
'ဇ',
'ဈ',
'ဉ',
'ည',
'ဋ',
'ဌ',
'ဍ',
'ဎ',
'ဏ',
'တ',
'ထ',
'ဒ',
'ဓ',
'န',
'ပ',
'ဖ',
'ဗ',
'ဘ',
'မ',
'ယ',
'ရ',
'လ',
'ဝ',
'သ',
'ဟ',
'ဠ',
'အ',
]


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    discount_line_ids = fields.One2many('account.invoice.discount', \
                                        'invoice_id', \
                                        string='Discount Lines', \
                                        states={'draft': [('readonly', False)]}\
                                        )
    custom_tax_line_ids = fields.One2many('account.invoice.custom_tax', \
                                        'invoice_id', \
                                        string='Taxes Lines', \
                                        states={'draft': [('readonly', False)]}\
                                        )
    date_shipmented = fields.Char(null = True)
    disc = fields.Float(default = 0, string = 'Commission')
    disc_type = fields.Selection([
                                    ('amt', 'Kyat'),
                                    ('perce', '%')
                                ], string = 'Type', default = 'perce')

    encoders = {
        "unicode": encodings.UnicodeEncoding(),
        "zawgyi": encodings.ZawgyiEncoding(),
        "wininnwa": encodings.WininnwaEncoding(),
    }

    def check_use_outstanding_payment_or_not(self, account_payment_id):
        # Currenly not used
        domain = [
                  ('payment_id.communication', '=', False),
                  ('partner_id', '=', self.env['res.partner']._find_accounting_partner(self.partner_id).id),
                  ('reconciled', '=', False),
                  ('move_id.state', '=', 'posted'),
                  '|',
                  '&', ('amount_residual_currency', '!=', 0.0), ('currency_id', '!=', None),
                  '&', ('amount_residual_currency', '=', 0.0), '&', ('currency_id', '=', None),
                  ('amount_residual', '!=', 0.0)]
        lines = self.env['account.move.line'].search(domain)
        outstandings = [ i.payment_id.id for i in lines]
        print('outstanding', outstandings, [i for i in account_payment_id if i.id in outstandings])
        return True if len( [i for i in account_payment_id if i.id in outstandings]) > 0 else False

    def convert_payments(self, payments_widget, payment_id):
        print('payment_id', payment_id)
        if payments_widget == 'false': return False
        payments_widget = json.loads(payments_widget)
        finalList = []
        total_outstanding_amount = 0

        for p in range(len(payments_widget['content'])):
            current_dict = payments_widget['content'][p]
            payment = self.env['account.payment'].search([('id', '=', current_dict['account_payment_id'])])
            print('outstanding_payment.invoice_ids', payment.invoice_ids)
            print('current', self)
            for invoice in payment.invoice_ids:
                payments = json.loads(invoice.payments_widget)['content']
                bills = [i for i in payments if i['account_payment_id'] == payment.id]
                print('bills', bills)
                if len(bills) == 0: continue
                print('--< ', invoice, self, invoice.id < self.id)
                if invoice.id != self.id and invoice.id < self.id:
                    total_outstanding_amount += sum([i['amount'] for i in bills])

            vals = {
                'name': 'ႀကိဳတင္ယူေငြ',
                'amount': payment.amount,
                'rest_amount': payment.amount - total_outstanding_amount,
                'type': 'outstanding' if payment.is_outstanding == True else 'normal'
            }
            finalList.append(vals)
        print('finalList', finalList)
        # d
        return finalList


    @api.model
    def convert(self, word):
        if not word: return
        char = word
        char1 = 'ေ'
        char2 = '်'
        char3 = '႐'
        s = list(char)

        for j in range(len(s)):
            if s[j] == char2:
                s[j] = '္ '
        char = "".join(s)
        final = self.convert_zawgyi(char, 'unicode', 'zawgyi')
        s = list(final)
        i = 0
        test = 'ၾ'
        test = test.encode('utf-8')
        if platform.system() == 'Linux':  # Only for linux
            while (i + 1 <= len(s)):
                if s[i] == char1 and (i + 1) < len(s) and s[i + 1] in myan_code:
                    _logger.info('Changed order of Characters')
                    s[i], s[i + 1] = s[i + 1], s[i]
                    i += 1
                i += 1

            for k in range(len(s)):
                if s[k] == char1 and (k + 1) < len(s) and s[k + 1].encode('utf-8') == test:
                    _logger.info('Replaced Character')
                    s[k] = '  ေ'
                elif s[k] == char1 and (k + 1) < len(s) and s[k + 1] == char3:
                    _logger.info('Replaced Character < ----- ')
                    s[k] = '  ေ'

        final = "".join(s)
        _logger.info('Final Result -> %s | In List -> %s'%(final, s))
        return final

    def convert_zawgyi(self, text, fromenc='unicode', toenc='zawgyi'):
        if fromenc not in self.encoders:
            raise NotImplementedError("Unsupported encoding: %s" % fromenc)

        if toenc not in self.encoders:
            raise NotImplementedError("Unsupported encoding: %s" % toenc)

        fromencoder = self.encoders[fromenc]
        toencoder = self.encoders[toenc]
        iterator = language.MorphoSyllableBreak(text=text, encoding=fromencoder)

        otext = ""
        for syllable in iterator:
            full_syllable = syllable['syllable']
            if len(syllable) == 1:  # unmatched text, no need to convert
                otext += full_syllable
                continue

            if full_syllable in fromencoder.reverse_table:  # Direct mapping
                key = fromencoder.reverse_table[full_syllable]
                key = key[: key.find('_')] if '_' in key else key  # remove _part
                otext += toencoder.table[key]
                continue

            otext += self.convert_syllable(syllable, fromenc, toenc)
        return otext

    def convert_syllable(self, syllable, fromenc, toenc):
        fromencoder = self.encoders[fromenc]
        toencoder = self.encoders[toenc]

        for part in syllable.keys():
            if part == 'syllable': continue  # noqa skip complete syllable

            key = fromencoder.reverse_table[syllable[part]]
            key = key[:key.find('_')] if '_' in key else key  # remove _part

            if part == "consonant":
                if key == "na":
                    key += self.choose_na_variant(syllable)
                if key == "ra":
                    key += self.choose_ra_variant(syllable)
                if key == "nnya":
                    key += self.choose_nnya_variant(syllable)
            elif part == "yapin":
                key += self.choose_yapin_variant(syllable)
            elif part == "yayit":
                key += self.choose_yayit_variant(syllable)
            elif part == "uVowel":
                key += self.choose_uvowel_variant(syllable)
            elif part == "aaVowel":
                key += self.choose_aavowel_variant(syllable)
            elif part == "dotBelow":
                key += self.choose_dot_below_variant(syllable)

            syllable[part] = key

        if 'uVowel' in syllable and 'hatoh' in syllable:
            syllable['hatoh'] = syllable['hatoh'] + '-' + syllable['uVowel']
            del syllable['uVowel']
        if 'wasway' in syllable and 'hatoh' in syllable:
            syllable['wasway'] = syllable['wasway'] + '-' + syllable['hatoh']
            del syllable['hatoh']

        osyllable = ""
        # collect codepoints in syllable, in correct syllable order
        for part in toencoder.syllable_parts:
            if part not in syllable: continue  # noqa

            try:
                key = syllable[part]
                osyllable += toencoder.table[key]
            except Exception:
                pass
        return osyllable

    def is_wide_consonant(self, char):
        WIDE_CONSONANTS = [
            "ka", "gha", "ca", "cha", "nya", "nna", "ta", "tha", "bha", "ya", "la",
            "sa", "ha", "a", "greatSa"
        ]
        return char in WIDE_CONSONANTS

    def is_lower_consonant(self, char):
        LOWER_CONSONANTS = [
            "nya",
            "na",
            "ra",  # ... more
        ]
        return char in LOWER_CONSONANTS

    def has_lower_marks(self, syllable, filters=[]):
        MAKRS = ["stack", "wasway", "yapin", "yayit", "hatoh", "uVowel"]
        for mark in [m for m in MAKRS if m not in filters]:
            if mark in syllable:
                return True
        return False

    def has_upper_marks(self, syllable, filters=[]):
        MAKRS = ["kinzi", "yapin", "iVowel", "aiVowel", "anusvara"]
        for mark in [m for m in MAKRS if m not in filters]:
            if mark in syllable:
                return True
        return False

    def choose_ra_variant(self, syllable):
        key = "_alt" if self.has_lower_marks(syllable, ["hatoh"]) else ""
        return key

    def choose_na_variant(self, syllable):
        key = "_alt" if self.has_lower_marks(syllable) else ""
        return key

    def choose_nnya_variant(self, syllable):
        key = "_alt" if self.has_lower_marks(syllable) else ""
        return key

    def choose_uvowel_variant(self, syllable):
        key = "_tall" if self.has_lower_marks(syllable, ["uVowel", "hatoh"]) else ""
        return key

    def choose_aavowel_variant(self, syllable):
        _C = ["kha", "gha", "nga", "da", "dha", "pa", "wa"]

        key = ""
        if syllable['consonant'] in _C:
            for c in ['yapin', 'yayit', 'wasway', 'hatoh']:
                if c in syllable:
                    break
            else:
                key += '_tall'

        return key

    def choose_yayit_variant(self, syllable):
        key = "_wide" if self.is_wide_consonant(syllable['consonant']) else "_narrow"
        key += "_lower" if self.has_lower_marks(syllable, ["yayit", "uVowel"]) else ""
        key += "_upper" if self.has_upper_marks(syllable, ["yayit"]) else ""
        return key

    def choose_yapin_variant(self, syllable):
        key = "_alt" if self.has_lower_marks(syllable, ["yapin", "uVowel"]) else ""
        return key

    def choose_dot_below_variant(self, syllable):
        key = ""

        if syllable['consonant'] == "na":
            key += "_alt"
        elif syllable['consonant'] == "ra":
            key += "_alt_alt"
        elif "uVowel" in syllable:
            key += "_alt_alt" if 'yayit' in syllable else '_alt'
        elif "yapin" in syllable:
            key += "_alt"
        elif "wasway" in syllable:
            key += "_alt_alt"

        return key

    def _prepare_invoice_line_from_po_line(self, line):
        #
        #   Add new value (shipment_id)
        #
        data = super(AccountInvoice, self)._prepare_invoice_line_from_po_line(line)
        data['shipment_id'] = line.order_id.picking_ids.filtered(lambda p: p.state == 'done').filtered(lambda p: line.product_id.id in [i.id for i in p.move_lines.mapped('product_id')]) or False
        return data

    @api.one
    @api.depends(
        'state', 'currency_id', 'invoice_line_ids.price_subtotal',
        'move_id.line_ids.amount_residual',
        'move_id.line_ids.currency_id')
    def _compute_residual(self):
        round_curr = self.currency_id.round
        res = super(AccountInvoice, self)._compute_residual()
        return res
        # if self.custom_tax_line_ids and self.type in ['out_invoice', 'out_refund'] and  self.state in ['draft', 'open']:
        #   self.residual += sum(round_curr(line.amount) for line in self.custom_tax_line_ids)
        #   self.residual_signed += sum(round_curr(line.amount) for line in self.custom_tax_line_ids)
        #   self.residual_company_signed += sum(round_curr(line.amount) for line in self.custom_tax_line_ids)

    def preprae_for_lines(self, obj_type, from_invoice = False):
        #
        #   Set default fields(tax or discount lines) for invoice
        #
        # po_names = [ 'Fees', 'Labor b', 'Wages', 'Advance payment']
        # so_names = [ 'Chinpat', 'Gone ni - PiNan - Salt', 'Repair and labor b']
        po_names = [ 'တန်ဆာခ ', 'အလုပ်သမားခ ']
        so_names = [ 'ချင်းပတ်', 'ဂုံနီ-ပီနံ-ဆား', 'ပြုပြင်ခနှင့် လုပ်အားခ']
        discount_line_ids, custom_tax_line_ids = [], []
        if obj_type == 'in_invoice':
            result = [discount_line_ids.append((0, 0, {"name": value})) for value in po_names]
            # result = [obj.discount_line_ids.create({'name': value, 'invoice_id': obj.id}) for value in po_names]
        elif obj_type == 'out_invoice':
            if not from_invoice:
                result = [custom_tax_line_ids.append((0, 0, {"name": value})) for value in so_names]
            else:
                if self._context.get('active_model') == 'sale.order' and self._context.get('active_id'):
                    sale_order = self.env['sale.order'].search([('id', '=', self._context.get('active_id'))])
                    result = [custom_tax_line_ids.append((0, 0, {"name": line.name, 'amount': line.amount})) for line in sale_order.custom_tax_line_ids]
                else:
                    result = [custom_tax_line_ids.append((0, 0, {"name": value})) for value in so_names]
            # result = [obj.custom_tax_line_ids.create({'name': value, 'invoice_id': obj.id}) for value in so_names]

        return [discount_line_ids, custom_tax_line_ids]

    @api.onchange('discount_line_ids', 'custom_tax_line_ids', 'disc', 'disc_type')
    def _compute_amount__(self):
        round_curr = self.currency_id.round
        amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)

        amount_tax = sum(round_curr(line.amount) for line in self.tax_line_ids) + \
                     sum(round_curr(line.amount) for line in self.custom_tax_line_ids)

        amount_total = (self.amount_untaxed + self.amount_tax)
        origin_discount = 0

        if not self.disc_type:
            self.disc_type = 'perce'

        if self.disc_type == 'amt':
            origin_discount = self.disc + sum(round_curr(line.amount) for line in self.discount_line_ids)
        elif self.disc_type == 'perce':
            origin_discount = (amount_total - (amount_total * (1 - self.disc / 100))) + sum(round_curr(line.amount) for line in self.discount_line_ids)

        if (amount_untaxed + amount_tax):
            amount_total = (amount_untaxed + amount_tax) - origin_discount

        amount_total_company_signed = amount_total
        amount_untaxed_signed = amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1

        self.amount_untaxed = amount_untaxed
        self.amount_tax = amount_tax
        self.amount_total = amount_total
        self.origin_discount = origin_discount
        self.amount_total_company_signed = amount_total_company_signed * sign
        self.amount_total_signed = amount_total * sign
        self.amount_untaxed_signed = amount_untaxed_signed * sign


    def _get_origin_discount(self, origin):
        origin_ = self.env['sale.order'].search([('name', '=', origin)]) or self.env['purchase.order'].search([('name', '=', origin)])
        return origin_.global_discount

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'tax_line_ids.amount_rounding',
                 'currency_id', 'company_id', 'date_invoice', 'type', 'discount_line_ids', 'custom_tax_line_ids')
    def _compute_amount(self):
        round_curr = self.currency_id.round
        amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
        amount_tax = sum(round_curr(line.amount_total) for line in self.tax_line_ids) + sum(round_curr(line.amount) for line in self.custom_tax_line_ids)
        amount_total = amount_untaxed + amount_tax
        amount_total_company_signed = amount_total
        amount_untaxed_signed = amount_untaxed
        if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
            currency_id = self.currency_id.with_context(date=self.date_invoice)
            amount_total_company_signed = currency_id.compute(amount_total, self.company_id.currency_id)
            amount_untaxed_signed = currency_id.compute(amount_untaxed, self.company_id.currency_id)
        sign = self.type in ['in_refund', 'out_refund'] and -1 or 1
        origin_discount = 0
        if not self.origin_discount:
            if self.origin and amount_total and not self.check_previous_discount_invoice()[0]:
                origin_discount = self._get_origin_discount(self.origin)
        else:
            origin_discount = self.origin_discount

        if amount_total > origin_discount:
            amount_total = (amount_untaxed + amount_tax) - origin_discount

        amount_total_company_signed = amount_total_company_signed * sign
        amount_total_signed = amount_total * sign
        amount_untaxed_signed = amount_untaxed_signed * sign

        self.amount_untaxed =  amount_untaxed
        self.amount_tax =  amount_tax
        self.amount_total =  amount_total
        self.amount_untaxed_signed =  amount_untaxed_signed
        self.amount_total_company_signed =  amount_total_company_signed
        self.amount_total_signed =  amount_total_signed

    @api.model
    def default_get(self, default_fields):
        res = super(AccountInvoice, self).default_get(default_fields)
        disc_lines, custom_tax_lines = self.preprae_for_lines(res.get('type') or 'out_invoice', from_invoice = True)
        res['discount_line_ids'] = disc_lines
        res['custom_tax_line_ids'] = custom_tax_lines
        return res

    @api.model
    def create(self, values):
        res = super(AccountInvoice, self).create(values)
        #
        #   Recalc values
        #
        res._compute_amount__()
        if res.origin and res.type not in ['in_refund']:
            order = self.env['purchase.order'] if res.origin.startswith('P') else self.env['sale.order']
            origin_Order = order.search([('name', '=', res.origin)])
            date_shipmented = origin_Order.picking_ids and origin_Order.picking_ids[0].scheduled_date.split()[0] or ''
            if date_shipmented:
                date_shipmented = '%s/%s/%s'%(date_shipmented.split('-')[1], date_shipmented.split('-')[2], date_shipmented.split('-')[0])
                res.date_shipmented = date_shipmented
        # d
        return res

    @api.one
    def change_move_stae(self, move, state):
        #
        #   Change state of a move line.(Make it as modiftable)
        #
        move.write({'state': state})

    def prepare_debit_item(self, move, credit_or_debit, value, acc):
        return {
            'name': '{} Tax: {}'.format('SO', move.name),
            'partner_id': move.partner_id.id,
            'move_id': move.id,
            'debit': value if credit_or_debit == 'credit' else 0,
            'credit': value if credit_or_debit == 'debit' else 0,
            'journal_id': move.journal_id.id,
            'account_id': acc.id,
            'invoice_id': self.id,
            'ref': move.name
        }

    @api.multi
    def action_move_create(self):
        #
        #   Create move line for taxes
        #
        res = super(AccountInvoice, self).action_move_create()
        AccountMoveLines = self.env['account.move.line']
        for inv in self:
            round_curr = inv.currency_id.round
            move = AccountMoveLines.search([('invoice_id', '=', self.id)], limit = 1).move_id
            lines = AccountMoveLines.search([('move_id', '=', move.id)])
            credit_or_debit = ""
            if len(inv.custom_tax_line_ids) > 0:
                #
                #   For SO
                #
                main_acc = lines.filtered(lambda l: l.account_id.id in [7, 13])
                acc = self.env['account.account'].search([('name', '=', 'Tax Paid')])
                value = sum(round_curr(line.amount) for line in inv.custom_tax_line_ids)
                credit_or_debit = 'credit' if main_acc.credit else 'debit'
                if credit_or_debit == 'debit':
                    self.change_move_stae(move, 'draft')
                    main_acc.debit += value
                    self.change_move_stae(move, 'posted')
                vals = self.prepare_debit_item(move, credit_or_debit, value, acc)
                debit_item = AccountMoveLines.create(vals)

        return res

