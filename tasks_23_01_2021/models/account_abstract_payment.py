from odoo import models, fields, api

class AccountAbstractModel(models.AbstractModel):
    _inherit = "account.abstract.payment"

    # @api.model
    # def _compute_total_invoices_amount(self):
    #     print("_compute_total_invoices_amount 2")
    #     """ Compute the sum of the residual of invoices, expressed in the payment currency """
    #     total = super(AccountAbstractModel, self)._compute_total_invoices_amount()
    #     print('total', total, self, self.invoice_ids)
    #     for inv in self.invoice_ids:
    #         round_curr = inv.currency_id.round
    #         if len(inv.custom_tax_line_ids) > 0 and inv.type in ['out_invoice', 'out_refund']:
    #             total += sum(round_curr(line.amount) for line in inv.custom_tax_line_ids)
    #     return abs(total)