from odoo import models, fields, api

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	custom_tax_line_ids = fields.One2many('account.invoice.custom_tax', \
									'sale_id', \
									string='Taxes Lines', \
									states={'draft': [('readonly', False)]}\
									)

	@api.model
	def default_get(self, default_fields):
		#
		#	Perpare for default custom tax lines
		#
		res = super(SaleOrder, self).default_get(default_fields)
		disc_lines, custom_tax_lines = self.env['account.invoice'].preprae_for_lines('out_invoice')
		res['custom_tax_line_ids'] = custom_tax_lines
		return res

	@api.model
	def create(self, values):
		print('values', values)
		res = super(SaleOrder, self).create(values)
		# d
		round_curr = res.currency_id.round
		res.amount_tax += sum(round_curr(line.amount) for line in res.custom_tax_line_ids)
		res.amount_total += sum(round_curr(line.amount) for line in res.custom_tax_line_ids)

		return res

	@api.depends('order_line.price_total')
	def _amount_all(self):
		"""
		Compute the total amounts of the SO.
		"""
		for order in self:
			round_curr = order.currency_id.round
			amount_untaxed = amount_tax = 0.0
			for line in order.order_line:
				amount_untaxed += line.price_subtotal
				amount_tax += line.price_tax
			if order.amount_untaxed:
				amount_tax += sum(round_curr(line.amount) for line in order.custom_tax_line_ids)
			order.update({
				'amount_untaxed': amount_untaxed,
				'amount_tax': amount_tax,
				'amount_total': (amount_untaxed + amount_tax) - order.global_discount,
			})

	@api.onchange('custom_tax_line_ids', 'order_line')
	def _compute_custom_tax_line_ids(self):
		#
		#	Add  to global discount of SO
		#
		for rec in self:
			round_curr = rec.currency_id.round
			if rec.amount_untaxed:
				rec.amount_tax += sum(round_curr(line.amount) for line in rec.custom_tax_line_ids)
			rec._amount_all()