# -*- coding: utf-8 -*-
{
    'name': "Tasks_23_01_2021",
    'description': """
        Mya Annawar Tasks      ( Date : 23.01.2021 )

        ( ၃ ) Quantity တ ြင္ Require ျဖဳတ္ေပးရန္။
        ( ၆ ) Good Receipt တွင် ဦးရေ ထည့်ပေးရန်။
        ( ၁၁) Vendor Bill ၏ amount ေအာက္တ ြင္ တန္ဆာခ ၊ အလုပ္သမားခ ၊ လုပ္အားခ ႏွင့္ ျကိုတင္ယူေင ြ
        တို႕ကို Fixed ထားေပးပါ။
        ( ၁၂) Invoice ၏ amount ေအာက္တ ြင္ ခ်င္းပတ္ ၊ ဂံုနီ - ပီနံ - ဆား ၊ ျပဳျပင္ႏွင့္ လုပ္သားခ တို႕ကို Fixed
        ထားေပးပါ။
    """,

    'author': "Kanaung Myanmar Software Solution",
    'category': 'Tasks',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
                    'base',
                    'web',
                    'purchase',
                    'stock',
                    'account',
                    'sale',
                    'ATL_so_discount',          # For origin_discount field of SO
                    'ATL_po_global_discount',   # For origin_discount field of PO
                    'saleorder_line_extension'  # For number field
                ],

    # always loaded
    'application': True,
    'data': [
        'security/ir.model.access.csv',
        'views/fonts.xml',
        'views/stock_picking.xml',
        "views/account_invoice.xml",
        "views/account_payment.xml",
        'views/sale_order.xml',
        'views/paper_format.xml',
        'views/invoice_report.xml'
    ]
}