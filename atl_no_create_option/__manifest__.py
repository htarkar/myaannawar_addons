# -*- coding: utf-8 -*-
{
    'name': "No Create Options(Myaannawar)",

    'description': """
        Long description of module's purpose
    """,

    'author': "Kanaung Myanmar",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': [
                'base','purchase','product','account','payment','stock_account','sale'],

    # always loaded
    'data': [
        'views/no_create_option_view.xml',
        'views/no_create_accounting_view.xml',
        'views/no_create_inventory_view.xml',
        'views/no_create_sale_view.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
    'installable': True,
}