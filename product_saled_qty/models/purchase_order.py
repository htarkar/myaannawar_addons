from odoo import fields, models, api

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    def _compute_total_saled(self):
        self.saled_line_count = sum([i.product_qty for i in self.find_sale_lines()])

    saled_line_count = fields.Integer(compute = '_compute_total_saled' )

    def find_sale_lines(self, product_id = False):
        StockMoves = self.env["stock.move"]
        #
        #   When filter movelines, there are three reasons to filter
        #   1: must be sale products
        #   2: must be same stock picking
        #   3: don't have returned stock picking
        move_lines = StockMoves.search([
            ('product_id', 'in', [product_id] if product_id else [o.product_id.id for o in self.order_line] ),
            ('sale_line_id', '!=', False),
            ("state", '=', 'done')
        ]).filtered(lambda m: \
                        m.location_dest_id.usage == 'customer' and \
                        m.sale_line_id.purchase_order.id in [p.id for p in self.picking_ids] and \
                        m.sale_line_id.qty_invoiced > 0 and \
                        m.id not in [s.origin_returned_move_id.id for s in
                                     self.env['stock.move'].search([('origin_returned_move_id', '!=', False)])]
                    )

        return move_lines

    def action_view_saled_qtys(self):
        ids = [i.id for i in self.find_sale_lines()]
        return {
            'name': 'Saled Products',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'res_model': 'stock.move',
            'domain': "[('id', 'in', %s)]"%(ids),
            'context': "{'group_by': 'product_id'}",
            'view_id': self.env.ref('product_saled_qty.view_saled_products').id,
            'search_view_id': self.env.ref('product_saled_qty.view_saled_products_search').id
        }