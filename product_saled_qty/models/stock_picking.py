from odoo import fields, models, api

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        if args is None:
            args = []
        pickings = self.browse()
        results = False
        if name :
            results = self.search(['|', ('name', '=', name), ('origin', '=', name)] + args, limit=limit)
        if not results:
            results = self.search(['|', ('name', operator, name), ('origin', operator, name)] + args, limit=limit)
        if not results and name:
            results = self.search([('state', '=', 'done'), ('sale_id', '=', False)]).filtered(lambda s: name.lower() in ' '.join([ p.product_id.product_tmpl_id.display_name.lower() for p in s.move_lines]))
        return results.name_get()

    def name_get(self):
        res = []
        for rec in self:
            res.append((rec.id, '%s - %s' %(rec.origin, rec.name)))
        return res