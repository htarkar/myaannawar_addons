from odoo import fields, models, api

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    def _get_picking_domains(self):
        return [('state', '=', 'done'), ('sale_id', '=', False)]

    stock_picking = fields.Many2one('stock.picking', 'Good Receive', required=True, domain = _get_picking_domains)

    def get_product_picking(self):
        return [i.id for i in self.env['stock.picking'].search(
                [('state', '=', 'done'), ('sale_id', '=', False)]).filtered(
                lambda s: self.product_id.id in [p.product_id.id for p in s.move_lines])]

    @api.onchange('stock_picking', 'product_id')
    def getPurchaseOrders(self):
        domain = [('id', 'in', [p.id for p in self.stock_picking.move_lines.mapped('product_id')] )] if self.stock_picking else []
        if not self.product_id.id:
            picking_domain = [('id', 'in', [i.id for i in self.env['stock.picking'].search([('state', '=', 'done'), ('sale_id', '=', False)])])]
        else:
            picking_domain = [('id', 'in', self.get_product_picking() if len(self.get_product_picking()) > 0 else [])]

        return {'domain': {'product_id': domain, 'stock_picking': picking_domain }}

