# -*- coding: utf-8 -*-

from odoo import models, fields, api


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    saled_qty = fields.Float(string = "Saled Qty", compute = '_computeProductSaledQty')

    def _computeProductSaledQty(self):
        for line in self:
            line.saled_qty = sum([i.product_qty for i in line.order_id.find_sale_lines(product_id = line.product_id.id)]) or 0
