from odoo import api, fields, models, _

class StockMove(models.Model):
    _inherit = "stock.move"

    sale_line_price_unit = fields.Float(string = 'Unit Price', related = 'sale_line_id.price_unit')
    sale_line_price_total = fields.Monetary(string = 'Total', related = 'sale_line_id.price_total')
    currency_id = fields.Many2one(string='Currency', related='sale_line_id.currency_id')