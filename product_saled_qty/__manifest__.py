# -*- coding: utf-8 -*-
{
    'name': "Trace Product Saled Qty",

    'author': "Kanaung Myanmar Software Solution",
    # 'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Purchase',
    'version': '11.0.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'purchase', 'sale', 'saleorder_line_extension'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/purchase_order.xml',
        # 'views/sale_order.xml',
        'views/stock_move.xml',
    ]
}
