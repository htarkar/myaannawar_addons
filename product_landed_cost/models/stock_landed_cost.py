# -*- coding: utf-8 -*-

from odoo import models, fields, api

class LandedCost(models.Model):
	_inherit = 'stock.landed.cost'

	@api.model
	def _set_default_acc(self):
		#
		#	Set one default acc
		#
		Journals = self.env['account.journal']
		if Journals.search([('name', '=', 'Stock Journal')]):
			return Journals.search([('name', '=', 'Stock Journal')]).id

	account_journal_id = fields.Many2one(
			'account.journal', 'Account Journal',
			required=True, 
			default = _set_default_acc,
			states={'done': [('readonly', True)]}
		)

	def is_fofo_line(self, move, product):
		#
		#	Check that a product's move line is fifo or not
		#
		fifo_line = product.stock_move_ids.filtered(lambda s: s.remaining_qty > 0).sorted(key = lambda s: s.id)[0]

		if fifo_line:
			return True if fifo_line.id == move.id else False
		else:
			return False

	@api.multi
	def button_validate(self):
		res = super(LandedCost, self).button_validate()
		#
		#	Use product's fifo cost if product's categ is fifo 
		#
		for line in self.valuation_adjustment_lines:
			if not line.product_id.product_tmpl_id.already_landed:
				price_unit = self.picking_ids.mapped('move_lines').filtered(lambda l: l.product_id.id == line.product_id.id and l.remaining_qty > 0).sorted(key = lambda n: n.id)
				if line.product_id.product_tmpl_id.categ_id.property_cost_method == 'fifo':
					price_unit = price_unit[0].price_unit
					if not self.is_fofo_line(line.move_id, line.product_id):
						#
						#	If product's categ is fifo method but this move_line is not fifo line	
						#
						continue
				else:
					price_unit = self.picking_ids.mapped('move_lines').filtered(lambda l: l.id == line.move_id.id).price_unit
					print('price_unit 2', price_unit)
				
				vals = { 
						'already_landed': True,
						'standard_price': price_unit,
						# 'list_price': line.product_id.product_tmpl_id.list_price + total_landed_cost,
					 }
				
				line.product_id.product_tmpl_id.write(vals)
		#
		#	When added landed cost, reset product template's already_landed value.
		#
		for product in self.valuation_adjustment_lines.mapped('product_id'):
			product.product_tmpl_id.write({ 'already_landed': False })

		return res