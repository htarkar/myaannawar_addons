# -*- coding: utf-8 -*-

from odoo import models, fields, api

class StockMove(models.Model):
	_inherit = 'stock.move'

	price_unit = fields.Float('Unit Price', \
							help="Technical field used to record the product cost set by the user during a picking confirmation (when costing "
							"method used is 'average price' or 'real'). Value given in company currency and in product uom.", \
							copy=False, \
							group_operator = False # Hide group by auto sum attribute 
							) 