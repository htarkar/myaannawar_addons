# -*- coding: utf-8 -*-

from . import product_template
from . import stock_landed_cost
from . import stock_move
from . import purchase_order