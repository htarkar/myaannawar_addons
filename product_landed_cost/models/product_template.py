# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ProductTemplate(models.Model):
	_inherit = 'product.template'

	already_landed = fields.Boolean(default = False)

	@api.multi
	def product_purchase_on_hand(self):
		#
		#	SHow on hand by remaining qty of purchase
		#
		ProductProduct = self.env['product.product']
		vals = {
			'name': 'Product Moves',
			'type': 'ir.actions.act_window',
			'view_mode': 'tree',
			'res_model': 'stock.move',
			'view_id': self.env.ref('product_landed_cost.stock_move_purchase_view_tree').id,
			'search_view_id': self.env.ref('product_landed_cost.view_move_purchase_view_search').id,
			'context': {'search_default_groupby_location_id': 1, 'search_default_internal_loc': 1},
			'domain': "[\
						('product_id', '=', %d), \
						'|', \
						('price_unit', '<', 0), \
						('remaining_qty', '>', 0)]"%(ProductProduct.search([('product_tmpl_id', '=', self.id)]).id)
		}

		return vals
