# -*- coding: utf-8 -*-

from odoo import models, fields, api

class PurchaseOrder(models.Model):
	_inherit = 'purchase.order'

	@api.one
	def get_purchase_order_id(self, name):
		print("## Name -> ", name)
		return self.search([('name', '=', name)]).id or False
