odoo.define('product_landed_cost.EditableListRenderer', function(require) {
'use strict';
	var rpc = require('web.rpc');
	var ListRenderer = require('web.ListRenderer');

	ListRenderer.include({
		events: _.extend({}, ListRenderer.prototype.events, {
           'click .open_purchase_form': 'open_purchase_form'
        }),
	    open_purchase_form: function(e) {
	    	var self = this;
	    	var td = $(e.currentTarget);
	    	rpc.query({
                model: 'purchase.order',
                method: 'get_purchase_order_id',
                args: [td[0].parentElement, td[0].parentElement.children[3].innerHTML],
            }).then(function(result) {
            	console.log('result', result)
            	if(result != false) {
                    self.do_action({
                        name: 'Purchase Order',
                        type: 'ir.actions.act_window',
                       	res_model: 'purchase.order',
                        views: [[false, 'form']],
                        view_mode: 'form',
                        res_id: result[0]
                    });
                }
            });
	    },
	});

});