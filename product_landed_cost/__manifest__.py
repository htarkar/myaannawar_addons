# -*- coding: utf-8 -*-
{
    'name': "Product's Landed Cost",
    'description': """
        Add additional landed cost to related product.
    """,

    'author': "Kanaung Myanmar Software Solution",
    'category': 'Inventory',
    'version': '0.1',
    'depends': [
                    'base',
                    'stock',
                    'stock_landed_costs'
                ],
    'data': [
        'assets.xml',
        'views/stock_move.xml',
        'views/product_purchase_views.xml'
    ]
}