# -*- coding: utf-8 -*-
{
    'name': "Age Calculate Confirmation(Myaannawar)",

    'description': """
        User Setting Confirmation. 
    """,

    'author': "Kanaung Myanmar",
    'website': "http://www.yourcompany.com",

    'category': 'Uncategorized',
    'version': '0.1',

    'depends': ['base'],

    'data': [
        'views/language_selection.xml',
    ],
    'qweb': [
        'static/src/xml/base.xml',
        ],
    'demo': [
    
    ],
    'installable': True,
}