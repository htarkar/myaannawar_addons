from odoo import models, fields, api
import datetime

class PurchaseOrderLine(models.Model):
	_inherit = 'purchase.order.line'

	@api.onchange('product_id')
	def onchange_product_id_warning_update(self):
		#
		# update the functin to limit product_uom many2one field  
		#
		return {'domain': {
								'product_uom': [('id', 'in', [self.product_id.uom_id.id, self.product_id.uom_po_id.id])]
							}}

    			
class Picking(models.Model):
	_inherit = "stock.picking"

	@api.multi
	def change_product_cost_price(self):
		print('change_product_cost_price')
		"""
			When user purchase an product or products with an order,
			set the cost_price of the product with purchased_price from order
			if that product is purchased for the first time
		"""
		move_products_qty_done = {i.product_id.id:i.quantity_done for i in self.move_lines}
		move_products_qtys = [True if move_products_qty_done[i] == 0 else False for i in move_products_qty_done]
		for line in self.move_lines:
			if all(move_products_qtys) == False:	# If user not valid all products in shipment
				if move_products_qty_done.get(line.product_id.id) == 0:
					continue
			
			origin_product = line.product_id
			if self.location_dest_id.name == 'Stock' and \
				self.location_dest_id.usage == 'internal' and \
				self.origin and \
				not self.origin.startswith('Return'):		# not in so/po return
				print("################")
				if not origin_product.standard_price or origin_product.qty_available <= 0:
					vals = {
						'standard_price': line.price_unit
					}
					if not origin_product.list_price or origin_product.list_price in [0, 1]:
						vals.update({
							'list_price': line.price_unit,
						})
					origin_product.write(vals)

	@api.multi
	def button_validate(self):
		res = super(Picking, self).button_validate()		# return origin method
		self.change_product_cost_price()
		return res
		

