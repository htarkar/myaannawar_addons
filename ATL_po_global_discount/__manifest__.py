{
    'name': "PO Global Discount(Myannawar)",
    'author': "Kanaung Myanmar Software Solution",
    'website': "",
    'category': 'PurchaseOrder',
    'version': '0.1',
    'depends': [
                'base',
                'purchase',
                'account',
                'stock',
                'ATL_so_discount'
                ],

    'data': [
        'views/views.xml',
        'views/pod_sequence.xml',
        'views/po_setting_view.xml',
        #'views/stock_move_lines_view.xml',
        #'views/stock_move.xml',
        'views/change_po_global_discount.xml'
    ],
}
