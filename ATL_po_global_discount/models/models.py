from odoo import models, fields, api, _
from ast import literal_eval
from odoo.tools import float_is_zero
from odoo.exceptions import UserError

class PoSetting(models.TransientModel):
	_inherit = "res.config.settings"

	_use_global_discount = fields.Boolean(default = False, string = 'PO Commission')
	_g_d_acc = fields.Many2one('account.account', string = 'Account')

	@api.onchange('_use_global_discount')
	def set_g_d_acc(self):
		#
		#	Reset acc 
		#
		if not self._use_global_discount:
			self._g_d_acc = False

	@api.model
	def get_values(self):
		#
		#	Get values of setting when refresh
		#
		res = super(PoSetting, self).get_values()
		params = self.env['ir.config_parameter'].sudo()
		res.update(
			_use_global_discount=params.get_param('ATL_po_global_discount._use_global_discount', default=False),
			_g_d_acc=literal_eval(params.get_param('ATL_po_global_discount._g_d_acc', default='False'))
		)
		return res

	def set_values(self):
		#
		#	Save setting when user make something changes
		#
		super(PoSetting, self).set_values()
		discount = self._use_global_discount
		discount_acc = self._g_d_acc.id

		self.env['ir.config_parameter'].sudo().set_param('ATL_po_global_discount._use_global_discount', discount)
		self.env['ir.config_parameter'].sudo().set_param('ATL_po_global_discount._g_d_acc', discount_acc)
		

class PurchaseOrder(models.Model):
	_inherit = 'purchase.order'

	disc = fields.Float(default = 0, string = 'Commission')
	disc_type = fields.Selection([
									('amt', 'Kyat'), 
									('perce', '%')
								], string = 'Type', default = 'perce')
	global_discount = fields.Float(null = True, string = 'Commission')

	@api.onchange('date_order')
	def _change_also_lines(self):
		#
		#	When change PO's date, change also lines' dates
		#
		if self.order_line:
			for line in self.order_line:
				line.date_planned = self.date_order

	@api.onchange('disc_type', 'disc')
	def _compute_global_discount(self):
		#
		#	To Compute global discount of PO
		#
		for rec in self:
			amount_total = rec.amount_untaxed + rec.amount_tax
			rec.disc = rec.disc

			if rec.disc_type == 'amt':
				rec.global_discount = rec.disc
			elif rec.disc_type == 'perce':
				rec.global_discount = amount_total - (amount_total * (1 - rec.disc / 100))

			rec._amount_all()

	@api.model
	def create(self, values):
		res = super(PurchaseOrder, self).create(values)
		res._compute_global_discount()
		return res

	@api.multi
	def write(self, values):
		res = super(PurchaseOrder, self).write(values)

		if 'disc' in values or 'disc_type' in values:
			rec = self
			amount_total = rec.amount_untaxed + rec.amount_tax
			if rec.disc_type == 'amt':
				rec.global_discount = rec.disc
			elif rec.disc_type == 'perce':
				rec.global_discount = amount_total - (amount_total * (1 - rec.disc / 100))

			rec._amount_all()
		return res

	@api.depends('order_line.price_total')
	def _amount_all(self): 
		#
		#	Recalc PO Total Amount
		#
		for order in self:
			amount_untaxed = amount_tax = 0.0
			for line in order.order_line:
				amount_untaxed += line.price_subtotal
				amount_tax += line.price_tax

			order.update({
				'amount_untaxed': order.currency_id.round(amount_untaxed),
				'amount_tax': order.currency_id.round(amount_tax),
				'amount_total': (amount_untaxed + amount_tax) - order.global_discount
			})

class PurchaseOrderLine(models.Model):
	_inherit = 'purchase.order.line'

	@api.onchange('product_id')
	def onchange_product_id(self):
		#
		#	Set product's default date base on PO's order date
		#
		res = super(PurchaseOrderLine, self).onchange_product_id()
		self.date_planned = self.order_id.date_order
		return res

class AccountInvoice(models.Model):
	_inherit = "account.invoice"

	origin_discount = fields.Float(default = 0.0, string = 'Commission')
	date_invoice = fields.Date(string='Invoice Date',
		readonly=True, states={'draft': [('readonly', False)]}, index=True,
		help="Keep empty to use the current date", copy=False, default = fields.Date.today)
	
	def _recompute_po_amount_again(self):
		#
		#	Recompute all_amount when user change discount amount
		#
		self.amount_total = (self.amount_untaxed + self.amount_tax) - self.origin_discount
		amount_total_company_signed = self.amount_total
		sign = self.type in ['in_refund', 'out_refund'] and -1 or 1

		self.amount_total_company_signed = amount_total_company_signed * sign
		self.amount_total_signed = self.amount_total * sign

	def _get_origin_discount(self, origin):
		origin_ = self.env['sale.order'].search([('name', '=', origin)]) or self.env['purchase.order'].search([('name', '=', origin)])
		return origin_.global_discount

	@api.one
	def check_previous_discount_invoice(self):
		#
		#	Check discount on previous invoices
		#
		if type(self.id) != int:
			return False
		previous_invoice = self.search([('origin', '=', self.origin), ('state', 'not in', ['cancel']), ('id', '!=', self.id)])
		if previous_invoice:
			for invoice in previous_invoice:
				if invoice.origin_discount != 0:
					return True
		return False

	@api.one
	@api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'tax_line_ids.amount_rounding',
				 'currency_id', 'company_id', 'date_invoice', 'type')
	def _compute_amount(self):
		round_curr = self.currency_id.round
		amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
		amount_tax = sum(round_curr(line.amount_total) for line in self.tax_line_ids)

		amount_total = amount_untaxed + amount_tax
		amount_total_company_signed = amount_total
		amount_untaxed_signed = amount_untaxed
		if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
			currency_id = self.currency_id.with_context(date=self.date_invoice)
			amount_total_company_signed = currency_id.compute(amount_total, self.company_id.currency_id)
			amount_untaxed_signed = currency_id.compute(amount_untaxed, self.company_id.currency_id)
		sign = self.type in ['in_refund', 'out_refund'] and -1 or 1

		origin_discount = 0
		if not self.origin_discount:
			if self.origin and self.amount_total and not self.check_previous_discount_invoice()[0]:
				origin_discount = self._get_origin_discount(self.origin)
		else:
			origin_discount = self.origin_discount

		if amount_total > origin_discount:
			amount_total = (amount_untaxed + amount_tax) - origin_discount

		amount_total_company_signed = amount_total_company_signed * sign
		amount_total_signed = amount_total * sign
		amount_untaxed_signed = amount_untaxed_signed * sign

		self.amount_untaxed =  amount_untaxed
		self.amount_tax =  amount_tax
		self.amount_total =  amount_total
		self.amount_untaxed_signed =  amount_untaxed_signed
		self.amount_total_company_signed =  amount_total_company_signed
		self.amount_total_signed =  amount_total_signed
		
	@api.depends('origin')
	def _calc_origin_discount(self):
		origin_PO = self.env['purchase.order'].search([('name', '=', self.origin)])
		previous_invoice = self.search([('origin', '=', self.origin), ('state', 'not in', ['cancel']), ('id', '!=', self.id)])
		if previous_invoice:
			existed = False
			for invoice in previous_invoice:
				if invoice.origin_discount != 0:
					existed = True
			if not existed:
				self.origin_discount = origin_PO.global_discount
				self._compute_amount()

		else:
			self.origin_discount = origin_PO.global_discount
			self._compute_amount()

	@api.model
	def create(self, values):
		res = super(AccountInvoice, self).create(values)
		res._calc_origin_discount()
		# d
		return res

	def _create_credit_item_for_po_discount(self, move, credit_value, credit_or_debit = None):
		#
		#	Create discount credit entry line
		#
		params = self.env['ir.config_parameter'].sudo()
		po_discount_acc = literal_eval(params.get_param('ATL_po_global_discount._g_d_acc', default='False'))

		if not po_discount_acc:
			raise UserError(_('''There is no account defined for PO\' Comission.Before make a payment, setting up an account in 
								Purchases -> Configuration -> Settings -> PO Commission.'''))

		return {
			'name': '{} Global Discount: {}'.format('PO', move.name),
			'partner_id': move.partner_id.id,
			'move_id': move.id,
			'debit': credit_value if credit_or_debit == 'debit' else 0,
			'credit': credit_value if credit_or_debit == 'credit' else 0,
			'journal_id': move.journal_id.id,
			'account_id': po_discount_acc,
			'invoice_id': self.id,
			'ref': move.name
		}

	@api.one
	def _po_get_payable_acc(self):
		#
		#	Return payable account
		#
		payable_type = self.env['account.account.type'].search([
																	'&',
																	('type', '=', 'payable'),
																	('name', '=', 'Payable')
																], limit = 1)
		payable_account = self.env['account.account'].search([
																('user_type_id', '=', payable_type.id),
																('internal_type', '=', 'payable'),
																('name', '=', 'Account Payable')
															], limit = 1)

		return payable_account

	def check_po_discount_item(self, line, inv, move):
		#
		#	Create discount item on related journal (Account Payable (credit or debit))
		#	Return credit or debit 
		#
		credit_or_debit = ""
		payable_acc = self._po_get_payable_acc()[0]
		if payable_acc:
			#
			# For PO
			#
			payable_line = [i for i in line if i.account_id.id == payable_acc.id]
			credit_or_debit = 'credit' if payable_line[0]['credit'] else 'debit'
			self.change_move_line_stae(move, 'draft')
			if credit_or_debit == 'credit':
				payable_line[0].credit = payable_line[0].credit - inv.origin_discount
			else:
				payable_line[0].debit = payable_line[0].debit - inv.origin_discount
			self.change_move_line_stae(move, 'posted')
		return credit_or_debit

	@api.one
	def change_move_line_stae(self, move, state):
		#
		#	Change state of a move line.(Make it as modiftable)
		#
		move.write({'state': state})

	@api.multi
	def action_move_create(self):
		#
		#	Create move line for globale discount
		#
		res = super(AccountInvoice, self).action_move_create()
		AccountMoveLines = self.env['account.move.line']
		for inv in self:
			move = AccountMoveLines.search([('invoice_id', '=', self.id)], limit = 1).move_id
			line = AccountMoveLines.search([('move_id', '=', move.id)])
			credit_or_debit = ""
			if inv.origin_discount and inv.type in ['in_invoice', 'in_refund']:
				#
				#	For PO Global Discount
				#
				credit_or_debit = self.check_po_discount_item(line, inv, move)
				credit_item = AccountMoveLines.create(inv._create_credit_item_for_po_discount(move, inv.origin_discount, credit_or_debit))
		return res

	# Load all unsold PO lines
	@api.onchange('purchase_id')
	def purchase_order_change(self):
		if not self.purchase_id:
			return {}
		if not self.partner_id:
			self.partner_id = self.purchase_id.partner_id.id

		vendor_ref = self.purchase_id.partner_ref
		if vendor_ref and (not self.reference or (
				vendor_ref + ", " not in self.reference and not self.reference.endswith(vendor_ref))):
			self.reference = ", ".join([self.reference, vendor_ref]) if self.reference else vendor_ref

		new_lines = self.env['account.invoice.line']
		for line in self.purchase_id.order_line - self.invoice_line_ids.mapped('purchase_line_id'):
			data = self._prepare_invoice_line_from_po_line(line)
			product_product = self.env['product.product'].search([('id', '=', data['product_id'])])
			if len(product_product) > 0 and data['quantity'] == 0 and product_product.product_tmpl_id.purchase_method == 'receive':
				#
				#	Skip unshipped Products
				#
				continue
			new_line = new_lines.new(data)
			new_line._set_additional_fields(self)
			new_lines += new_line
		self.invoice_line_ids += new_lines
		self.payment_term_id = self.purchase_id.payment_term_id
		self.env.context = dict(self.env.context, from_purchase_order_change=True)
		self.origin_discount = self.purchase_id.global_discount
		self.purchase_id = False
		return {}

	@api.multi
	def change_po_global_discount(self):
		vals = {
			'invoice_id': self.id,
			'discount_amount': self.origin_discount
		}
		item = self.env['change.po.global.discount'].create(vals)
		return {
			'type': 'ir.actions.act_window',
			'res_model': 'change.po.global.discount',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': self.env.ref('ATL_po_global_discount.view_change_po_global_discount').id,
			'res_id': item.id,
			'target': 'new'
		}

class StockMoveLine(models.Model):
	_inherit = "stock.move.line"

	date_expected = fields.Datetime(related = 'move_id.date_expected', string = 'Accounting Date', store = True)
	parent_origin = fields.Char(related = 'picking_id.origin', string = 'Source Document', store = True)