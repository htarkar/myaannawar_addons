from odoo import api, fields, models

class StockScrap(models.Model):
	_inherit = 'stock.scrap'

	# @api.multi
	# def _find_pcs_uom(self):
	# 	#
	# 	#	Default PCS uom
	# 	#
	# 	return self.env['product.uom'].search([('name', '=', 'Pcs'), ('uom_type', '=', 'reference')]).id

	# product_uom_id = fields.Many2one(
	# 	'product.uom', 'Unit of Measure',
	# 	default = _find_pcs_uom,
	# 	required=True, states={'done': [('readonly', True)]})

	# @api.onchange('product_id')
	# def onchange_product_id(self):
	# 	#
	# 	#	To stop changing product's uom
	# 	#
	# 	pass

	def _prepare_move_values(self):
		#
		#	For Back Scraping
		#
		self.ensure_one()
		return {
			'name': self.name,
			'origin': self.origin or self.picking_id.name or self.name,
			'product_id': self.product_id.id,
			'product_uom': self.product_uom_id.id,
			'product_uom_qty': self.scrap_qty,
			'location_id': self.location_id.id,
			'scrapped': True,
			'location_dest_id': self.scrap_location_id.id,
			'date_expected': self.date_expected,
			'move_line_ids': [(0, 0, {'product_id': self.product_id.id,
									  'date_expected': self.date_expected,
									  'product_uom_id': self.product_uom_id.id, 
									  'qty_done': self.scrap_qty,
									  'location_id': self.location_id.id, 
									  'location_dest_id': self.scrap_location_id.id,
									  'package_id': self.package_id.id, 
									  'owner_id': self.owner_id.id,
									  'lot_id': self.lot_id.id, })],
			# 'restrict_partner_id': self.owner_id.id,
			'picking_id': self.picking_id.id
		}