from odoo import models, fields, api

class ChangePODiscount(models.TransientModel):
	_name = "change.po.global.discount"

	discount_amount = fields.Float('Commission', required = True, default = 0)
	invoice_id = fields.Many2one('account.invoice')

	@api.one
	def change_po_discount(self):
		#
		#	Update invoice global discount
		#
		self.invoice_id.origin_discount = self.discount_amount
		self.invoice_id._recompute_po_amount_again()
