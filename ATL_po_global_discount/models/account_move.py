from odoo import api, fields, models

class Account_Move(models.Model):
	_inherit = "account.move"
	
	@api.multi
	def assert_balanced(self):
		if not self.ids:
			return True
		prec = self.env['decimal.precision'].precision_get('Account')

		# self._cr.execute("""\
  #           SELECT      move_id, abs(sum(debit) - sum(credit))
  #           FROM        account_move_line
  #           WHERE       move_id in %s
  #           GROUP BY    move_id
  #           HAVING      abs(sum(debit) - sum(credit)) > %s
  #           """, (tuple(self.ids), 10 ** (-max(5, prec))))

		# res = self._cr.fetchone()
		# if res:
  #           raise UserError(
  #               _("Cannot create unbalanced journal entry.") +
  #               "\n\n{}{}".format(_('Difference debit - credit: '), res[1])
		# 	)
		return True