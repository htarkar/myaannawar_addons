from odoo import models, fields, api

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    partner_ref = fields.Char(string='Partner Reference', copy=False)

    @api.model
    def create(self, values):
        res = super(StockPicking, self).create(values)
        if res.location_dest_id.usage == 'internal' and res.picking_type_id.code == 'incoming' and res.origin != False and res.origin.startswith('PO'):
            res.write({'partner_ref': self.env['purchase.order'].search([('name', '=', res.origin)]).partner_ref})
        return res
