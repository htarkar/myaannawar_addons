from odoo import models, fields, api

class StockMove(models.Model):
    _inherit = 'stock.move'

    partner_ref = fields.Char(string='Partner Reference', copy=False)
    
    def _get_new_picking_values(self):
        values = super(StockMove, self)._get_new_picking_values()
        values.update({
            'partner_ref': self.partner_ref
        })
        return values

    @api.multi
    def write(self, values):
    	return super(StockMove, self).write(values)