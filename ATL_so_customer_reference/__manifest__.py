# -*- coding: utf-8 -*-
{
    'name': "Sale Order Customer Reference(Myaannawar)",
    'author': "Kanaung Myanmar Software Solution",
    'website': "http://www.yourcompany.com",
    'category': 'Sale',
    'version': '0.1',
    'depends': [
                    'base',
                    'sale',
                    'stock',
                    'account'
                ],
    'data': [
        'views/account_invoice.xml',
        'views/sale_order.xml',
        'views/stock_picking.xml',
    ],
}