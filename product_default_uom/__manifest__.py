# -*- coding: utf-8 -*-
{
    'name': "Product's Default UOM",
    'description': """
        This module allow user to set a default uom when initial creating products.
    """,

    'author': "Kanaung Myanmar Software Solution",
    'category': 'Product',
    'version': '0.1',

    'depends': [
                'base',
                'stock'
                ],

    'data': [
        'views/config_setting.xml'
    ],
}