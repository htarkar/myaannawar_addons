from odoo import models, fields, api
from ast import literal_eval

class UOMSetting(models.TransientModel):
	_inherit = "res.config.settings"

	default_uom = fields.Many2one('product.uom', string = 'Product Unit of Measure')

	@api.model
	def get_values(self):
		#
		#	Get values of setting when refresh
		#
		res = super(UOMSetting, self).get_values()
		params = self.env['ir.config_parameter'].sudo()

		res.update(
			default_uom=literal_eval(params.get_param('product_default_uom.default_uom', default='False'))
		)
		return res

	def set_values(self):
		#
		#	Save setting when user make something changes
		#
		super(UOMSetting, self).set_values()
		default_uom_id = self.default_uom.id

		self.env['ir.config_parameter'].sudo().set_param('product_default_uom.default_uom', default_uom_id)
