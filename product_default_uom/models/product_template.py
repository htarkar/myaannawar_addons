from odoo import models, fields, api
from ast import literal_eval

class ProductTemplate(models.Model):
	_inherit = "product.template"

	def _get_default_setting_uom_id(self):
		#
		#	Modify uom to default most rating rom
		#
		params = self.env['ir.config_parameter'].sudo()
		default_uom = literal_eval(params.get_param('product_default_uom.default_uom', default='False'))
		return default_uom if default_uom else self.env['product.uom'].search([])[0].id
		
	uom_id = fields.Many2one(
		'product.uom', 'Unit of Measure',
		default=_get_default_setting_uom_id, required=True,
		help="Default Unit of Measure used for all stock operation.")
	uom_po_id = fields.Many2one(
		'product.uom', 'Purchase Unit of Measure',
		default=_get_default_setting_uom_id, required=True,
		help="Default Unit of Measure used for purchase orders. It must be in the same category than the default unit of measure.")
