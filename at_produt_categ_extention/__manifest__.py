# -*- coding: utf-8 -*-
{
    'name': "Product categ extention(Myaannawar)",

    'description': """
       Product_Category_extention.
    """,

    'author': "Kanaung Myanmar",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'product','stock_account'],

    # always loaded
    'data': [
        'views.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
    ],
    'installable': True,
}