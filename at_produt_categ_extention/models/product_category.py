from odoo import models, fields, api
from odoo.exceptions import ValidationError
from collections import Counter

class ProductName(models.Model):
	_inherit = "product.template"

	categ_id = fields.Many2one(
		'product.category', 'Internal Category',
		change_default=True, default="",
		required=True, help="Select category for the current product")

	def _get_default_rating_uom_id(self):
		#
		#	Modify uom to default most rating rom
		#
		return self.env["product.uom"].search([], limit=1, order='rating desc').id
		
	uom_id = fields.Many2one(
		'product.uom', 'Unit of Measure',
		default=_get_default_rating_uom_id, required=True,
		help="Default Unit of Measure used for all stock operation.")
	uom_po_id = fields.Many2one(
		'product.uom', 'Purchase Unit of Measure',
		default=_get_default_rating_uom_id, required=True,
		help="Default Unit of Measure used for purchase orders. It must be in the same category than the default unit of measure.")

	@api.one
	def calc_uom_in_product(self, uom):
		products = self.search([('uom_id', '=', uom.id)])
		uom.rating = len(products)

	@api.model
	def create(self, vals):
		vals.update({'name': vals.get('name').strip()})
		res = super(ProductName, self).create(vals)
		uom = self.env['product.uom'].search([('id', '=', res.uom_id.id)])
		self.calc_uom_in_product(uom)	# product count
		return res

	def uom_rating(self, uom, increase = None):
		#
		#	Rebalance uom rating ratio
		#
		ProductUOM = self.env['product.uom']
		uom = ProductUOM.search([('id', '=', uom)])
		# print('uom.name', uom.name)
		self.calc_uom_in_product(uom)	# product count

	@api.multi
	def write(self, values):
		old_uom_po_id = self.uom_po_id.id
		old_uom_id = self.uom_po_id.id
		if 'name' in values:
			values.update({'name': values.get('name').strip()})

		record = super(ProductName, self).write(values)
		self.uom_rating(old_uom_id)
		self.uom_rating(old_uom_po_id)

		if 'uom_id' in values or 'uom_po_id' in values:
			uom_id = values.get('uom_id') or False
			uom_po_id = values.get('uom_po_id') or False
			if uom_po_id == uom_id:
				self.uom_rating(uom_id, increase = True)
			else:
				if uom_id:
					self.uom_rating(uom_id, increase = True)
				if uom_po_id:
					self.uom_rating(uom_po_id, increase = True)
		
		return record

	@api.one
	@api.constrains('name')   
	def _check_name(self):
		old_name = self.name
		domain = [('name','=', self.name)]
		name_recs = self.search(domain)

		if len(name_recs) > 1:            
			raise ValidationError('Product Must Be Unique!!!!')

	##### Remove space from Product Names ####
	# def remove_space(self):
	# 	o_name = self.name
	# 	print("Nam1____", o_name)
	# 	pt = self.env['product.template'].search([])
	# 	for i in pt:
	# 		sp_name = i.name.strip()
	# 		i.write({'name': sp_name})
		
class UnitOfMeaure(models.Model):
	_inherit = 'product.uom'
	_order = "rating desc"

	# sequence = fields.Integer(default=10, string='Sequence')
	###### Calculating used unit of measure in product ######
	@api.multi
	def get_rating(self):
		product = self.env['product.template'].search([])
		uom_ids = [i.uom_po_id.id for i in product]
		cou = Counter(uom_ids)
		for rec in cou:
			uom = self.search([('id', '=', rec)])
			uom.rating = cou[rec]

	rating = fields.Integer(null = True, compute ="get_rating", store=True, string="Products")

	@api.multi
	def _check_name(self, name): 
		name_list = {}
		name_recs = self.search([])
		for i in name_recs:
			name_list[i.name.lower().strip()] = i.id
		if name.lower() in name_list:
			raise ValidationError('The UOM named "{}" is already exist and we don\'t allow to have two uoms with same name.'.\
								format(self.search([('id', '=', name_list[name.lower()])]).name))

	@api.model
	def create(self, values):
		if 'name' in values:
			self._check_name(values.get('name'))
		rec = super(UnitOfMeaure, self).create(values)
		return rec

	@api.multi
	def write(self, values):
		if 'name' in values:
			self._check_name(values.get('name'))
		rec = super(UnitOfMeaure, self).write(values)
		return rec

class ProductCategory(models.Model):
	_name = "product.category"
	_inherit = "product.category"

	def _default_id(self):
		return self.env['product.removal'].search([('id', '=', 1)])

	removal_strategy_id = fields.Many2one('product.removal', default=_default_id)
	property_account_income_categ_id = fields.Many2one('account.account', string="Income Account", oldname="property_account_income_categ",
		domain=[('deprecated', '=', False)],company_dependent=True,
		help="This account will be used when validating a customer invoice.", required=True)
	property_account_expense_categ_id = fields.Many2one('account.account', company_dependent=True,
		string="Expense Account", oldname="property_account_expense_categ",
		domain=[('deprecated', '=', False)], required=True,
		help="The expense is accounted for when a vendor bill is validated, except in anglo-saxon accounting with perpetual inventory valuation in which case the expense (Cost of Goods Sold account) is recognized at the customer invoice validation.")

	property_stock_valuation_account_id = fields.Many2one(
		'account.account', 'Stock Valuation Account', company_dependent=True,
		domain=[('deprecated', '=', False)], required=True,
		help="When real-time inventory valuation is enabled on a product, this account will hold the current value of the products.",)
	property_stock_journal = fields.Many2one(
		'account.journal', 'Stock Journal', company_dependent=True, required=True,
		help="When doing real-time inventory valuation, this is the Accounting Journal in which entries will be automatically posted when stock moves are processed.")

	property_cost_method = fields.Selection([
		('standard', 'Standard Price'),
		('fifo', 'First In First Out (FIFO)'),
		('average', 'Average Cost (AVCO)')], string='Costing Method',
		company_dependent=False, copy=True, required=True, default='fifo',
		help="""Standard Price: The products are valued at their standard cost defined on the product.
		Average Cost (AVCO): The products are valued at weighted average cost.
		First In First Out (FIFO): The products are valued supposing those that enter the company first will also leave it first.""")

	property_valuation = fields.Selection([
		('manual_periodic', 'Manual'),
		('real_time', 'Automated')], string='Inventory Valuation',
		company_dependent=False, copy=True, required=True, default='real_time',
		help="""Manual: The accounting entries to value the inventory are not posted automatically.
		Automated: An accounting entry is automatically created to value the inventory when a product enters or leaves the company.
		""")

	@api.one
	@api.constrains('name')    
	def _check_name(self): 
		domain = [('name','=',self.name)]
		name_recs = self.search(domain)
		
		if len(name_recs) > 1:
			raise ValidationError('Product Category Must Be Unique!!!!')

	@api.model
	def create(self, vals):
		res = super(ProductCategory, self).create(vals)
		if res.property_valuation == 'manual_periodic':
			raise ValidationError("Inventory Valutation must be Automated.")
		return res

	@api.multi
	def write(self, values):
		record = super(ProductCategory, self).write(values)
		if self.property_valuation != 'real_time':
			raise ValidationError("Inventory Valutation must be Automated.")
		return record






