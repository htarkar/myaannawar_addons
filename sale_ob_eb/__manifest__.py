# -*- coding: utf-8 -*-
{
    'name': "Opening Balance and Ending Balance of Sale",
    'author': "Kanaung Myanmar Software Solution",
    'category': 'Sale',
    'version': '0.1',
    'depends': [
                    'base',
                    'account'
                ],

    'data': [
        'views/account_payment.xml',
        'views/sequences.xml',
    ],
}