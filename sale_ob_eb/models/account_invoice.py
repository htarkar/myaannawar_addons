from odoo import models, api
from odoo.exceptions import UserError
from datetime import datetime

class account_invoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def action_invoice_open(self):
        # If not opening balance for today, the user will not be invoice or bill anything
        todayOB = self.env['account.payment'].search([
                                                            ('oe_date', '=', datetime.today().strftime('%Y-%m-%d')),
                                                            ('state', 'in', ['posted', 'sent', 'reconciled']),
                                                            ('payment_type', '=', 'opening')
                                                        ])

        if not todayOB:
            raise UserError('To make payments, please first create an opening balance for today.')

        return super(account_invoice, self).action_invoice_open()