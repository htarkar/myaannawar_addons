from odoo import fields, models, api, _
from odoo.exceptions import UserError, ValidationError

class account_payment(models.Model):
    _inherit = "account.payment"

    def _get_last_ending_balance(self):
        print('-->', self.env['account.payment'].search_count([('payment_type', '=', 'ending')]) > 0 and self.env['account.payment'].search([('payment_type', '=', 'ending')], order = 'id desc')[0].ending_amount or 0)
        return self.env['account.payment'].search_count([('payment_type', '=', 'ending')]) > 0 and self.env['account.payment'].search([('payment_type', '=', 'ending')], order = 'id desc')[0].ending_amount or 0

    payment_type = fields.Selection(selection_add=[
                                                    ('opening', 'Opening Balance'),
                                                    ('ending', 'Ending Balance')
                                                    ])
    opening_amount = fields.Monetary(string = 'Opening Amount', required = True, default = _get_last_ending_balance)
    ending_amount = fields.Monetary(string = 'Ending Amount', required = True)
    oe_date = fields.Date(string='Date', default = fields.Date.context_today, required=True, copy=False)

    @api.one
    @api.depends('invoice_ids', 'payment_type', 'partner_type', 'partner_id')
    def _compute_destination_account_id(self):
        res = super(account_payment, self)._compute_destination_account_id()
        if self.payment_type in ['opening', 'ending']:
            if not self.company_id.transfer_account_id.id:
                raise UserError(_('Transfter account not defined on the company.'))
            self.destination_account_id = self.company_id.transfer_account_id.id

        return res

    def _get_counterpart_move_line_vals(self, invoice=False):
        vals = super(account_payment, self)._get_counterpart_move_line_vals(invoice = invoice)
        if self.payment_type in ['opening', 'ending']:
            name = _("%s Balance"%(self.payment_type.capitalize()))
            vals['name'] = name

        return vals

    @api.multi
    def post(self):
        # Add new sequence code for OB and EB
        for rec in self:

            if rec.state != 'draft':
                raise UserError(_("Only a draft payment can be posted."))

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            # keep the name in case of a payment reset to draft
            if not rec.name:
                # Use the right sequence to set the name
                if rec.payment_type == 'transfer':
                    sequence_code = 'account.payment.transfer'
                elif rec.payment_type in ['opening', 'ending']:
                    sequence_code = 'account.payment.%s.balance'%(rec.payment_type)
                    rec.amount = self.opening_amount or self.ending_amount
                else:
                    if rec.partner_type == 'customer':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.customer.invoice'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.customer.refund'
                    if rec.partner_type == 'supplier':
                        if rec.payment_type == 'inbound':
                            sequence_code = 'account.payment.supplier.refund'
                        if rec.payment_type == 'outbound':
                            sequence_code = 'account.payment.supplier.invoice'
                rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(
                    sequence_code)
                if not rec.name and rec.payment_type != 'transfer':
                    raise UserError(_("You have to define a sequence for %s in your company.") % (sequence_code,))

            # Create the journal entry
            amount = rec.amount * (rec.payment_type in ('outbound', 'transfer', 'opening') and 1 or -1)

            move = rec._create_payment_entry(amount)
            persist_move_name = move.name

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(
                    lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()
                persist_move_name += self._get_move_name_transfer_separator() + transfer_debit_aml.move_id.name

            rec.write({'state': 'posted', 'move_name': persist_move_name})

        return True