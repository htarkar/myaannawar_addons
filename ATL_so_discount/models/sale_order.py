from odoo import models, fields, api

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	disc = fields.Float(default = 0, string = 'Comission')
	disc_type = fields.Selection([
									('amt', 'Kyat'), 
									('perce', '%')
								], string = 'Type', default = 'perce')
	global_discount = fields.Float(null = True, string = 'Comission')

	@api.onchange('disc_type', 'disc')
	def _compute_global_discount(self):
		#
		#	To Compute global discount of SO
		#
		for rec in self:
			rec.disc = rec.disc
			amount_total = rec.amount_tax + rec.amount_untaxed
			if rec.disc_type == 'amt':
				rec.global_discount = rec.disc
			elif rec.disc_type == 'perce':
				rec.global_discount = amount_total - (amount_total * (1 - rec.disc / 100))
			rec._amount_all()

	@api.depends('order_line.price_total')
	def _amount_all(self):
		"""
		Compute the total amounts of the SO.
		"""
		for order in self:
			amount_untaxed = amount_tax = 0.0
			for line in order.order_line:
				amount_untaxed += line.price_subtotal
				amount_tax += line.price_tax
			order.update({
				'amount_untaxed': amount_untaxed,
				'amount_tax': amount_tax,
				'amount_total': (amount_untaxed + amount_tax) - order.global_discount,
			})

	@api.model
	def create(self, values):
		res = super(SaleOrder, self).create(values)
		# print("values", values)
		if 'disc' in values or 'disc_type' in values:
			rec = res
			amount_total = rec.amount_tax + rec.amount_untaxed
			if rec.disc_type == 'amt':
				rec.global_discount = rec.disc
			elif rec.disc_type == 'perce':
				rec.global_discount = amount_total - (amount_total * (1 - rec.disc / 100))

			rec._amount_all()
		return res

	@api.multi
	def write(self, values):
		res = super(SaleOrder, self).write(values)

		if 'disc' in values or 'disc_type' in values:
			rec = self
			amount_total = rec.amount_tax + rec.amount_untaxed
			if rec.disc_type == 'amt':
				rec.global_discount = rec.disc
			elif rec.disc_type == 'perce':
				rec.global_discount = amount_total - (amount_total * (1 - rec.disc / 100))

			rec._amount_all()

		return res