from odoo import models, fields, api

class ChangeSODiscount(models.TransientModel):
	_name = "change.so.global.discount"

	discount_amount = fields.Float('Discount', required = True, default = 0)
	invoice_id = fields.Many2one('account.invoice')

	@api.one
	def change_so_discount(self):
		#
		#	Update invoice global discount
		#
		print('change_so_discount', self.discount_amount, self.invoice_id)
		self.invoice_id.origin_discount = self.discount_amount
		self.invoice_id._recompute_again()
