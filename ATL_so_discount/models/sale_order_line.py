from odoo import api, fields, models

class SaleOrderLine(models.Model):
    _inherit = "sale.order.line"

    @api.one
    def _order_pricelist(self):
        #
        #   Set sale order pricelist as default
        #
        self.product_pricelist = self.order_id.pricelist_id

    product_pricelist = fields.Many2one('product.pricelist', string='Pricelist', 
                                                             required=True, 
                                                             readonly=True, 
                                                             states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, 
                                                             help="Pricelist for current sale order line.", 
                                                            )

    price_compute_type = [
        ('discount', '%'),
        ('fix_price', 'Kyat')
    ]
    discount_type = fields.Selection(
            price_compute_type,
            default = "discount",
            string = "Type"
        )

    @api.onchange('product_id')
    def _compute_pricelist(self):
        self._order_pricelist()

    @api.onchange('product_pricelist')
    def _recompute_product_unit_price(self):
        #
        #   When sale order line's pricelist change, recompute price unit based on product_pricelist
        #
        self.price_unit = self.product_id.with_context(pricelist=self.product_pricelist.id).price
        

    @api.onchange('discount', 'discount_type')
    def _recompute_amount(self):
        for line in self:
            # print('line._get_discounted_price_unit()', line._get_discounted_price_unit())
            line.price_subtotal = line._get_discounted_price_unit()

    @api.depends('price_unit', 'discount', 'discount_type')
    def _get_price_reduce(self):
        for line in self:
            line.price_reduce = line._get_discounted_price_unit()

    def _get_discounted_price_unit(self, discount_type = None):
     	#
     	#	Calc sale order line discount
     	#
        self.ensure_one()
        discount_type = discount_type if discount_type else self.discount_type
        # print('------> ', self.discount, self.discount_type, self.price_unit, discount_type)
        if self.discount and discount_type == "fix_price":
            return self.price_unit - (self.discount or 0.0)
        else :
            return self.price_unit * (1 - self.discount / 100)
        return self.price_unit

    @api.depends('product_uom_qty', 'discount', 'price_unit', 'tax_id')
    def _compute_amount(self):
        # print('_compute_amount')
        """
        Compute the amounts of the SO line.
        """
        for line in self:
            price = line._get_discounted_price_unit()
            # print('sdf price', price)
            taxes = line.tax_id.compute_all(price, line.order_id.currency_id, line.product_uom_qty, product=line.product_id, partner=line.order_id.partner_shipping_id)
            # print('taxes', taxes)
            line.update({
                'price_tax': sum(t.get('amount', 0.0) for t in taxes.get('taxes', [])),
                'price_total': taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })

    @api.multi
    def _prepare_invoice_line(self, qty):
        res = super(SaleOrderLine, self)._prepare_invoice_line(qty)
        res['discount_type'] = self.discount_type
        return res

    @api.multi
    def write(self, values):
        if values.get("discount") or values.get('discount_type'):
            discount_type = values.get('discount_type') or self.discount_type
            self.price_subtotal = self._get_discounted_price_unit(discount_type)
        res = super(SaleOrderLine, self).write(values)
        return res

