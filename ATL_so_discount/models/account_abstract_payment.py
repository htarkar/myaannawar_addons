from odoo import models, fields, api

class account_abstract_payment(models.AbstractModel):
    _inherit = "account.abstract.payment"

    @api.model
    def _compute_total_invoices_amount(self):
        """ Compute the sum of the residual of invoices, expressed in the payment currency """
        payment_currency = self.currency_id or self.journal_id.currency_id or self.journal_id.company_id.currency_id or self.env.user.company_id.currency_id

        total = 0
        for inv in self.invoice_ids:
            if inv.currency_id == payment_currency:
                total += inv.residual_signed
            else:
                total += inv.company_currency_id.with_context(date=self.payment_date).compute(
                    inv.residual_company_signed, payment_currency)

            if inv.origin_discount and inv.type in ['out_invoice']:
                total -= inv.origin_discount
        return abs(total)