from odoo import models, fields, api

class AccountPayment(models.Model):
    _inherit = "account.payment"

    @api.one
    @api.depends('invoice_ids', 'amount', 'payment_date', 'currency_id')
    def _compute_payment_difference(self):
        if len(self.invoice_ids) == 0:
            return
        if self.invoice_ids[0].type in ['in_invoice', 'out_refund']:
            self.payment_difference = self.amount - self._compute_total_invoices_amount()
        else:
        	#
        	#	Update total invoices amount to include discount in order to prevent payment difference
        	#
            if self.invoice_ids[0].type not in ['in_refund']:
                self.payment_difference = (self._compute_total_invoices_amount() + self.invoice_ids[0].origin_discount or 0) - self.amount
            else:
                self.payment_difference = self._compute_total_invoices_amount() - self.amount
