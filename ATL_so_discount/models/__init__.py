from . import config_setting
from . import account_move
from . import account_invoice
from . import sale_order
from . import sale_order_line
from . import account_abstract_payment
from . import account_payment
from . import change_so_global_discount