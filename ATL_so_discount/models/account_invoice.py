from odoo import models, fields, api, _
from odoo.tools import float_is_zero
from ast import literal_eval
from odoo.exceptions import UserError

class AccountInvoice(models.Model):
	_inherit = "account.invoice"

	origin_discount = fields.Float(default = 0.0, string = 'Comission')

	def _get_origin_discount(self, origin):
		origin_ = self.env['sale.order'].search([('name', '=', origin)]) or self.env['purchase.order'].search([('name', '=', origin)])
		return origin_.global_discount

	def _recompute_again(self):
		#
		#	Recompute all_amount when user change discount amount
		#
		self.amount_total = (self.amount_untaxed + self.amount_tax) - self.origin_discount
		amount_total_company_signed = self.amount_total
		sign = self.type in ['in_refund', 'out_refund'] and -1 or 1

		self.amount_total_company_signed = amount_total_company_signed * sign
		self.amount_total_signed = self.amount_total * sign

	@api.one
	def check_previous_discount_invoice(self):
		#
		#	Check discount on previous invoices
		#
		if type(self.id) != int:
			return False
		previous_invoice = self.search([('origin', '=', self.origin), ('state', 'not in', ['cancel']), ('id', '!=', self.id)])
		if previous_invoice:
			for invoice in previous_invoice:
				if invoice.origin_discount != 0:
					return True
		return False

	@api.one
	@api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount', 'tax_line_ids.amount_rounding',
				 'currency_id', 'company_id', 'date_invoice', 'type')
	def _compute_amount(self):
		round_curr = self.currency_id.round
		amount_untaxed = sum(line.price_subtotal for line in self.invoice_line_ids)
		amount_tax = sum(round_curr(line.amount_total) for line in self.tax_line_ids)

		amount_total = amount_untaxed + amount_tax
		amount_total_company_signed = amount_total
		amount_untaxed_signed = amount_untaxed
		if self.currency_id and self.company_id and self.currency_id != self.company_id.currency_id:
			currency_id = self.currency_id.with_context(date=self.date_invoice)
			amount_total_company_signed = currency_id.compute(amount_total, self.company_id.currency_id)
			amount_untaxed_signed = currency_id.compute(amount_untaxed, self.company_id.currency_id)
		sign = self.type in ['in_refund', 'out_refund'] and -1 or 1

		origin_discount = 0
		if not self.origin_discount:
			if self.origin and self.amount_total and not self.check_previous_discount_invoice()[0]:
				origin_discount = self._get_origin_discount(self.origin)
		else:
			origin_discount = self.origin_discount

		if amount_total > origin_discount:
			amount_total = (amount_untaxed + amount_tax) - origin_discount

		amount_total_company_signed = amount_total_company_signed * sign
		amount_total_signed = amount_total * sign
		amount_untaxed_signed = amount_untaxed_signed * sign

		self.amount_untaxed =  amount_untaxed
		self.amount_tax =  amount_tax
		self.amount_total =  amount_total
		self.amount_untaxed_signed =  amount_untaxed_signed
		self.amount_total_company_signed =  amount_total_company_signed
		self.amount_total_signed =  amount_total_signed
		

	def _create_credit_item_for_so_discount(self, move, credit_value, credit_or_debit = None):
		#
		#	Create discount credit entry line
		#
		params = self.env['ir.config_parameter'].sudo()
		so_discount_acc = literal_eval(params.get_param('at_so_discount._so_d_acc', default='False'))

		if not so_discount_acc:
			raise UserError(_('''There is no account defined for SO\' Comission.Before make a payment, setting up an account in 
								Sales -> Configuration -> Settings -> So Comission.'''))
		return {
			'name': '{} Comission: {}'.format('SO', move.name),
			'partner_id': move.partner_id.id,
			'move_id': move.id,
			'debit': credit_value if credit_or_debit == 'debit' else 0,
			'credit': credit_value if credit_or_debit == 'credit' else 0,
			'journal_id': move.journal_id.id,
			'account_id': so_discount_acc,
			'invoice_id': self.id,
			'ref': move.name
		}

	@api.one
	def _so_get_receivable_acc(self):
		#
		#	Return payable account
		#
		receivable_type = self.env['account.account.type'].search([
																	'&',
																	('type', '=', 'receivable'),
																	('name', '=', 'Receivable')
																], limit = 1)
		receivable_account = self.env['account.account'].search([
																('user_type_id', '=', receivable_type.id),
																('internal_type', '=', 'receivable'),
																('name', '=', 'Account Receivable')
															], limit = 1)

		return receivable_account

	def check_so_discount_item(self, line, inv, move):
		#
		#	Create discount item on related journal (Account Receivable (credit or debit))
		#	Return credit or debit 
		#
		credit_or_debit = ""
		receivable_acc = self._so_get_receivable_acc()[0]
		if receivable_acc:
			#
			# For SO
			#
			# print('inv.origin_discount', inv.origin_discount)
			receivable_line = [i for i in line if i.account_id.id == receivable_acc.id]
			credit_or_debit = 'credit' if receivable_line[0].credit else 'debit'
			self.change_move_line_stae(move, 'draft')
			if credit_or_debit == 'credit':
				receivable_line[0].credit = receivable_line[0].credit - inv.origin_discount
			else:
				receivable_line[0].debit = receivable_line[0].debit - inv.origin_discount
			self.change_move_line_stae(move, 'posted')
		return credit_or_debit
	@api.one
	def change_move_line_stae(self, move, state):
		#
		#	Change state of a move line.(Make it as modiftable)
		#
		move.write({'state': state})

	@api.multi
	def action_move_create(self):
		#
		#	Create move line for globale discount
		#
		res = super(AccountInvoice, self).action_move_create()
		AccountMoveLines = self.env['account.move.line']
		for inv in self:
			move = AccountMoveLines.search([('invoice_id', '=', self.id)], limit = 1).move_id
			line = AccountMoveLines.search([('move_id', '=', move.id)])

			credit_or_debit = ""
			if inv.origin_discount and inv.type in ['out_invoice', 'out_refund']:
				#
				#	For SO Global DIscount
				#
				credit_or_debit = self.check_so_discount_item(line, inv, move)

				credit_item = AccountMoveLines.create(inv._create_credit_item_for_so_discount(move, inv.origin_discount, credit_or_debit))

		return res

	@api.multi
	def change_so_global_discount(self):
		vals = {
			'invoice_id': self.id,
			'discount_amount': self.origin_discount
		}
		item = self.env['change.so.global.discount'].create(vals)
		return {
			'type': 'ir.actions.act_window',
			'res_model': 'change.so.global.discount',
			'view_mode': 'form',
			'view_type': 'form',
			'view_id': self.env.ref('ATL_so_discount.view_change_so_global_discount').id,
			'res_id': item.id,
			'target': 'new'
		}

class AccountInvoiceLine(models.Model):
	_inherit = "account.invoice.line"

	price_compute_type = [
		('discount', '%'),
		('fix_price', 'Kyat')
	]
	discount_type = fields.Selection(
			price_compute_type,
			default = "discount",
			string = "Type"
		)
	def _get_discounted_price_unit(self):
		#
		#	Calc sale order line discount
		#
		self.ensure_one()
		if self.discount and self.discount_type == "fix_price":
			return self.price_unit - (self.discount or 0.0)
		else :
			return self.price_unit * (1 - self.discount / 100)
		return self.price_unit

	@api.one
	@api.depends('price_unit', 'discount', 'invoice_line_tax_ids', 'quantity',
		'product_id', 'invoice_id.partner_id', 'invoice_id.currency_id', 'invoice_id.company_id',
		'invoice_id.date_invoice', 'invoice_id.date')
	def _compute_price(self):
		currency = self.invoice_id and self.invoice_id.currency_id or None
		price = self._get_discounted_price_unit()
		# print('price 2', price, self.discount_type, self.discount)
		taxes = False
		if self.invoice_line_tax_ids:
			taxes = self.invoice_line_tax_ids.compute_all(price, currency, self.quantity, product=self.product_id, partner=self.invoice_id.partner_id)
		self.price_subtotal = price_subtotal_signed = taxes['total_excluded'] if taxes else self.quantity * price
		self.price_total = taxes['total_included'] if taxes else self.price_subtotal
		if self.invoice_id.currency_id and self.invoice_id.currency_id != self.invoice_id.company_id.currency_id:
			price_subtotal_signed = self.invoice_id.currency_id.with_context(date=self.invoice_id._get_currency_rate_date()).compute(price_subtotal_signed, self.invoice_id.company_id.currency_id)
		sign = self.invoice_id.type in ['in_refund', 'out_refund'] and -1 or 1
		self.price_subtotal_signed = price_subtotal_signed * sign

