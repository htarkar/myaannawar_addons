from odoo import models, fields, api
from ast import literal_eval

class PoSetting(models.TransientModel):
	_inherit = "res.config.settings"

	_use_so_global_discount = fields.Boolean(default = False, string = 'SO Comission')
	_so_d_acc = fields.Many2one('account.account', string = 'Account')

	@api.onchange('_use_so_global_discount')
	def set_g_d_acc(self):
		#
		#	Reset acc 
		#
		if not self._use_so_global_discount:
			self._so_d_acc = False

	@api.model
	def get_values(self):
		#
		#	Get values of setting when refresh
		#
		res = super(PoSetting, self).get_values()
		params = self.env['ir.config_parameter'].sudo()

		res.update(
			_use_so_global_discount=params.get_param('at_so_discount._use_so_global_discount', default=False),
			_so_d_acc=literal_eval(params.get_param('at_so_discount._so_d_acc', default='False'))
		)
		return res

	def set_values(self):
		#
		#	Save setting when user make something changes
		#
		super(PoSetting, self).set_values()
		discount = self._use_so_global_discount
		discount_acc = self._so_d_acc.id

		self.env['ir.config_parameter'].sudo().set_param('at_so_discount._use_so_global_discount', discount)
		self.env['ir.config_parameter'].sudo().set_param('at_so_discount._so_d_acc', discount_acc)
