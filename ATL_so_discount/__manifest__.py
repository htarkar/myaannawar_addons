{
    'name': "Get Discount on Sale Order(Myaannawar)",
    'author': "Kanaung Myanmar Software Solution",
    'website': "",
    'category': 'Sale',
    'version': '0.1',
    'depends': [
                    'base',
                    'sale',
                    'account'
                ],
    'data': [
        'views/sale_order_form.xml',
        'views/so_config_setting.xml',
        'views/invoice_form.xml',
        'views/change_so_global_discount.xml'
    ],
}