# -*- coding: utf-8 -*-

from odoo import models, fields, api

class DynamicFieldsLines(models.Model):
	_name = 'dynamic.fields.lines'

	model_id = fields.Many2one('ir.model', string = 'Parent Model')
	model_name = fields.Char(string = 'Model')
	model_description = fields.Char(string = 'Model')
	field_id = fields.Integer(string = 'Field ID')
	field_description = fields.Char(string = 'Field Desciption')
	field_name = fields.Char(string = 'Field Name')
	can_create = fields.Boolean(string = 'Create')
	can_create_edit = fields.Boolean(string = 'Create and Edit ... ')
	can_search = fields.Boolean(string = 'Search More ... ')
	can_open = fields.Boolean(string = 'Open')
	limit = fields.Integer(string = 'Limit')

	@api.multi
	@api.onchange('can_create', 'can_create_edit', 'can_search', 'can_open', 'limit')
	def set_attribute_of_field(self):
		vals = {'can_create': self.can_create, 'can_create_edit': self.can_create_edit, 'can_search': self.can_search, 'can_open': self.can_open, 'limit': self.limit}
		self.search([('model_id', '=', self.model_id.id), ('field_id', '=', self.field_id)]).write(vals)

