from . import product_template
from . import product_pricelist_item
from . import product_category
from . import product_uom

from . import sale_order
from . import sale_order_line

from . import purchase_order
from . import purchase_order_line

from . import res_partner
from . import res_country_state

from . import stock_warehouse
from . import stock_move
from . import stock_picking

from . import account_invoice
from . import account_invoice_line