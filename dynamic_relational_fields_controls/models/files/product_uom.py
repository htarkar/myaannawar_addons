# -*- coding: utf-8 -*-

from odoo import models, fields, api
from lxml import etree

class ProductUOM(models.Model):
	_inherit = 'product.uom'

	@api.model
	def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
		Lines = self.env['dynamic.fields.lines'].search([])
		res = super(ProductUOM, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu = True)
		doc = etree.XML(res['arch'])
		if view_type == 'form':            # Applies only for form view
			for node in doc.xpath("//field"):   
				field = Lines.filtered(lambda l: l.model_name == res['model'] and l.field_name == node.attrib['name']) or False
				node.set('options', "{\
										'create': %s, \
										'm2o_dialog': %s, \
										'create_edit': %s, \
										'search_more': %s, \
										'no_open': %s, \
										'limit': %d } " % ( \
																'false' if not field else str(field.can_create).lower(), \
																'false' if not field else str(field.can_create).lower(), \
																'true' if not field else str(field.can_create_edit).lower(), \
																'true' if not field else str(field.can_search).lower(), \
																'True' if not field else not field.can_open, \
																4 if not field else field.limit ))

			res['arch'] = etree.tostring(doc)
		return res
