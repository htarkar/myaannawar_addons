# -*- coding: utf-8 -*-

from odoo import models, fields, api
import string 

class DynamicFieldsOptions(models.Model):
	_name = 'dynamic.fields.options'
	_description = 'Relational Field Options'

	@api.model
	def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
		#
		#	Reload content when getting to tree_view
		#
		res = super(DynamicFieldsOptions, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu = True)

		if view_type == 'tree':            # Applies only for tree view
			self.find_allowed_models()

	def search_models_id(self, name_list):
		Model = self.env['ir.model']

		id_list =  []
		for name in name_list:
			model = Model.search([('model', '=', name)])
			id_list.append(model.id)

			# Create fields of allowed model
			self.get_model_fields(model)

		return id_list

	@api.model
	def find_allowed_models(self):
		#
		#	Return id of allowed model to used in 
		#
		return [('id', 'in', self.search_models_id([
														'product.template', 
														'product.category',
														'product.uom',
														'product.pricelist.item'

														'sale.order', 
														'sale.order.line', 

														'purchase.order', 
														'purchase.order.line',

														'res.partner',
														'res.country.state',

														'stock.warehouse',
														'stock.move',
														'stock.picking',

														'account.invoice',
														'account.invoice.line',
													]))]

	name = fields.Char()

	@api.multi
	def generate_records(self):
		#
		#	Generating new record and detect new one
		#
		self.find_allowed_models()
		
		return {
			'name': 'Relational Fields Controls',
			'type': 'ir.actions.act_window',
			'view_mode': 'tree',
			'res_model': 'dynamic.fields.lines',
			'target': 'current',
		}

	def prepare_line_value(self, field):
		#
		#	Create a line if not exist
		#	As Default, only close create option
		#
		vals = {
			'model_id': field.model_id.id,
			'model_name': field.model_id.model,
			'model_description': field.model_id.name,
			'field_id': field.id,
			'field_name': field.name,
			'field_description': field.field_description,
			'limit': 6,
			'can_create_edit': True,
			'can_search': True,
			'can_open': True,
			'can_create': False
		}
		return vals

	def get_line_value(self, field, model, lines):
		#
		#	Fetch value from existing line 
		#
		old_line = lines.search([('model_id', '=', model.id), ('field_id', '=', field.id)])
		vals = {
			'model_id': old_line.model_id.id,
			'model_name': old_line.model_id.model,
			'model_description': old_line.model_id.name,
			'field_id': old_line.field_id,
			'field_name': field.name,
			'field_description': field.field_description,
			'limit': old_line.limit,
			'can_create_edit': old_line.can_create_edit,
			'can_search': old_line.can_search,
			'can_open': old_line.can_open,
			'can_create': old_line.can_create
		}
		return vals

	def get_model_fields(self, model):
		#
		#	We will create one view field for an relational field of that model if they are not exist.
		#
		if not model:
			return

		SubLines = self.env['dynamic.fields.lines'].search([])
		line_vals = []
		fields = model.field_id.filtered(lambda f: f.ttype in [ 'many2one', 'one2many', 'many2many' ])

		result = [line_vals.append((0, 0, self.prepare_line_value(f))) \
				if f.id not in [i.field_id for i in SubLines.filtered(lambda m: m.model_id.id == model.id)] \
				else line_vals.append((0, 0, self.get_line_value(f, model, SubLines)))\
				for f in fields ]

		for line in line_vals:
			if SubLines.search_count([ \
										('field_id', '=', line[2]['field_id']), \
										('model_id', '=', line[2]['model_id'])
										]) <= 0:
				SubLines.create(line[2])


