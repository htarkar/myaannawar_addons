# -*- coding: utf-8 -*-
{
    'name': "Controls Options of Relational Fields(Myannawar)",
    'description': """
        Take a control of 

            create, 
            create_edit,
            m2o_dialog,             # Dialog Box
            search_more,
            no_open

        of relation fields of models
    """,

    'author': "Kanaung Myanmar Software Solution",
    'category': 'Fields',
    'version': '0.1',
    'depends': [
                    'base',
                    'web_m2x_options',
                    'product',
                    'sale',
                    'purchase',
                    'stock',
                ],

    'data': [
        'assets.xml',
        'views/dynamic_fields.xml',
        'security/ir.model.access.csv'
    ],
}