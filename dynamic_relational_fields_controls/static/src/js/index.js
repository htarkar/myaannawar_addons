odoo.define('dynamic_relational_fields_controls.EditableListRenderer', function(require) {
'use strict';

	var ListRenderer = require('web.ListRenderer');

	ListRenderer.include({
		events: _.extend({}, ListRenderer.prototype.events, {
           'click .listen_to_click_': 'highlight_row'
        }),
	    
	    _renderRow: function (record) { 
	    	const $td = this._super.apply(this, arguments);
	    	//
	    	// Only affected to specified model tree view
	    	//
	    	if(record.model == "dynamic.fields.lines") {
		    	$td.addClass('listen_to_click_')
		    	$td.css({ "background": "none", "border": "2px solid #fff"})
	    	}
	    	return $td;
	    },

	    highlight_row: function(e) {
	    	//
	    	//	Highlight on current row and remove active from last highlighted row
	    	//
	    	var td = $(e.currentTarget);
	    	td.addClass('highlighted')

	    	var $records = td.parent().children().filter('.highlighted')
	    	var $current = $records.filter('.o_selected_row')
	    	var $old = $records.not('.o_selected_row')

	    	if ($old) {
	    		$old.css({ 
	    					"background": "none", 
	    					"font-weight": "normal", 
	    					'border': "none" , 
	    					'box-sizing:': "unset"
	    				})
	    	}
	    	$current.css({
	    				 	"background": "#99fd99", 
	    				 	"font-weight": "bold", 
	    				 	'border': "2px solid #99fd99", 
	    				 	'box-sizing:': 
	    				 	"border-box"
	    				 })
	    	$old.removeClass('highlighted')
	    },
	});

});
